﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

namespace TCGClient
{
    public class DisableModelOnStart : StartupScript
    {
        // Declared public member fields and properties will show in the game studio

        public override void Start()
        {
            // Initialization of the script.
            //Entity.Get<ModelComponent>()?.Enabled = false;
            Entity.Remove<ModelComponent>();
        }
    }
}
