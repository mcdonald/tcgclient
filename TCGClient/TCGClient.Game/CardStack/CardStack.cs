﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

namespace TCGClient.CardStack
{
    public class CardStack : StartupScript
    {
        // Declared public member fields and properties will show in the game studio
        Entity StartPoint;
        Entity EndPoint;

        Entity[] ConnectionPoints;

        public int GetPointDefault => 60;

        public float VericalOffset = 0.001f;
        public float MaxDistance =0;

        public bool IsVertical = false;

        public Prefab CardPrefab;

        public override void Start()
        {
            // Initialization of the script.
            if (!IsVertical)
            {
                StartPoint = Entity.FindChild("StartPoint");

                EndPoint = Entity.FindChild("EndPoint");
            }

            //CardPrefab = Content.Load<Prefab>("_Main/Card/Card");

            if (CardPrefab == null)
                throw new Exception("Card Prefab not found");

            ConnectionPoints = new Entity[GetPointDefault];
            for(int i = 0; i < GetPointDefault; i++)
            {
                var instance = CardPrefab.Instantiate();
                ConnectionPoints[i] = instance[0];
                SceneSystem.SceneInstance.RootScene.Entities.Add(ConnectionPoints[i]);
                //ConnectionPoints[i].Transform.Parent = Entity.Transform;

            }
            UpdatePoints();
        }

        public void UpdatePoints()
        {

            int numOfPoints = ConnectionPoints.Length;
            float startPosX =0; 
            float Xoffset =0;

            // Entity.Get<TransformComponent>().GetWorldTransformation(out var startWorldPos, out _, out _);
            if (!IsVertical)
            {
                startPosX  = StartPoint.Transform.Position.X;
                //StartPoint.Transform.GetWorldTransformation(out startWorldPos, out _, out _);

                 Xoffset  = (EndPoint.Transform.Position.X - startPosX) / numOfPoints;
            }

            for (int i = 0; i < numOfPoints; i++)
            {
                Vector3 stackDisplacement = new Vector3();
                stackDisplacement.Y = i * VericalOffset;
                stackDisplacement.X = (i * Xoffset) + startPosX;
                stackDisplacement.Z = 0;

                Vector3.TransformCoordinate(ref stackDisplacement, ref Entity.Transform.WorldMatrix, out var result);
                var position = Entity.Transform.WorldMatrix.TranslationVector;
                position += stackDisplacement;
                ConnectionPoints[i].Transform.Position = position;
            }
        }
    }
}
