﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core;
using Xenko.Core.Mathematics;
using Xenko.Animations;
using Xenko.Input;
using Xenko.Engine;

namespace TCGClient
{
    public class SimpleRotor : SyncScript
    {
        [DataContract("Axis")]
        public enum Axises { X, Y , Z}

        public int Speed = 1;

        public Axises Axis;
        // Declared public member fields and properties will show in the game studio
        TransformComponent transform;
        public override void Start()
        {
            // Initialization of the script.
            transform = Entity.Get<TransformComponent>();
        }

        public override void Update()
        {
            

            var amount = MathUtil.DegreesToRadians((float)Game.UpdateTime.Elapsed.TotalSeconds) * Speed;
            Quaternion rotation = new Quaternion();

            switch (Axis)
            {
                case Axises.X:
                    Quaternion.RotationX(amount, out  rotation);
                    break;

                case Axises.Y:
                    Quaternion.RotationY(amount, out  rotation);
                    break;

                case Axises.Z:
                    Quaternion.RotationZ(amount, out  rotation);
                    break;

            }            

            transform.Rotation *= rotation;
           


            //DebugText.Print($"{eularRotation:F3}", new Int2(x: 50, y: 50));
            //transform.Rotate(new Vector3(0,(float)Game.UpdateTime.Elapsed.TotalSeconds, 0));
            //transform.Rotate( new Vector3(newY, 0, 0));

        }
    }
}
