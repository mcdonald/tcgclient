using Xenko.Engine;

namespace TCGClient
{
    class TCGClientApp
    {
        static void Main(string[] args)
        {
            using (var game = new Game())
            {
                game.Run();
            }
        }
    }
}
