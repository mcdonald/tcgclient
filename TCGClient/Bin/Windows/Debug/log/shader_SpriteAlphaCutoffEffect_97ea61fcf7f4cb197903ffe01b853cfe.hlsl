/**************************
***** Compiler Parameters *****
***************************
@P EffectName: SpriteAlphaCutoffEffect
***************************
****  ConstantBuffers  ****
***************************
cbuffer PerDraw [Size: 64]
@C    MatrixTransform_id73 => SpriteBase.MatrixTransform
***************************
******  Resources    ******
***************************
@R    PerDraw => PerDraw [Stage: Vertex, Slot: (-1--1)]
@R    Texture0_id14 => Texturing.Texture0 [Stage: Pixel, Slot: (-1--1)]
@R    Sampler_id42 => Texturing.Sampler [Stage: Pixel, Slot: (-1--1)]
***************************
*****     Sources     *****
***************************
@S    SpriteAlphaCutoff => 8ddf6f10f22aa5785a28ce1261f76957
@S    SpriteBase => e319c4777a964c3ebb8f82590e4e27f1
@S    ShaderBase => acbe3d4d44a046eede871176bee9c754
@S    ShaderBaseStream => a3a5bf8185f2a3d89972293f806430d3
@S    Texturing => e6daef0dd90a55f9549c6f5d291e61a5
@S    ColorUtility => 7cc858a121384cef612dee545b823e45
***************************
*****     Stages      *****
***************************
@G    Vertex => 508694534a0c2c22d16c97b374fe7fbe
@G    Pixel => d9675f5f1698db1d31010d1b38a96573
***************************
*************************/
const static bool TSRgb_id74 = false;
struct PS_STREAMS 
{
    float Swizzle_id77;
    float2 TexCoord_id62;
    float4 Color_id75;
    float4 ColorAdd_id76;
    float4 ColorTarget_id2;
};
struct PS_OUTPUT 
{
    float4 ColorTarget_id2 : SV_Target0;
};
struct PS_INPUT 
{
    float4 ShadingPosition_id0 : SV_Position;
    float4 Color_id75 : COLOR;
    float Swizzle_id77 : BATCH_SWIZZLE;
    float2 TexCoord_id62 : TEXCOORD0;
    float4 ColorAdd_id76 : COLOR1;
};
struct VS_STREAMS 
{
    float4 Position_id72;
    float4 Color_id75;
    float Swizzle_id77;
    float2 TexCoord_id62;
    float4 ColorAdd_id76;
    float4 ShadingPosition_id0;
};
struct VS_OUTPUT 
{
    float4 ShadingPosition_id0 : SV_Position;
    float4 Color_id75 : COLOR;
    float Swizzle_id77 : BATCH_SWIZZLE;
    float2 TexCoord_id62 : TEXCOORD0;
    float4 ColorAdd_id76 : COLOR1;
};
struct VS_INPUT 
{
    float4 Position_id72 : POSITION;
    float4 Color_id75 : COLOR;
    float Swizzle_id77 : BATCH_SWIZZLE;
    float2 TexCoord_id62 : TEXCOORD0;
    float4 ColorAdd_id76 : COLOR1;
};
cbuffer PerDraw 
{
    float4x4 MatrixTransform_id73;
};
cbuffer Globals 
{
    float2 Texture0TexelSize_id15;
    float2 Texture1TexelSize_id17;
    float2 Texture2TexelSize_id19;
    float2 Texture3TexelSize_id21;
    float2 Texture4TexelSize_id23;
    float2 Texture5TexelSize_id25;
    float2 Texture6TexelSize_id27;
    float2 Texture7TexelSize_id29;
    float2 Texture8TexelSize_id31;
    float2 Texture9TexelSize_id33;
};
Texture2D Texture0_id14;
Texture2D Texture1_id16;
Texture2D Texture2_id18;
Texture2D Texture3_id20;
Texture2D Texture4_id22;
Texture2D Texture5_id24;
Texture2D Texture6_id26;
Texture2D Texture7_id28;
Texture2D Texture8_id30;
Texture2D Texture9_id32;
TextureCube TextureCube0_id34;
TextureCube TextureCube1_id35;
TextureCube TextureCube2_id36;
TextureCube TextureCube3_id37;
Texture3D Texture3D0_id38;
Texture3D Texture3D1_id39;
Texture3D Texture3D2_id40;
Texture3D Texture3D3_id41;
SamplerState Sampler_id42;
SamplerState PointSampler_id43 
{
    Filter = MIN_MAG_MIP_POINT;
};
SamplerState LinearSampler_id44 
{
    Filter = MIN_MAG_MIP_LINEAR;
};
SamplerState LinearBorderSampler_id45 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Border;
    AddressV = Border;
};
SamplerComparisonState LinearClampCompareLessEqualSampler_id46 
{
    Filter = COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
    ComparisonFunc = LessEqual;
};
SamplerState AnisotropicSampler_id47 
{
    Filter = ANISOTROPIC;
};
SamplerState AnisotropicRepeatSampler_id48 
{
    Filter = ANISOTROPIC;
    AddressU = Wrap;
    AddressV = Wrap;
    MaxAnisotropy = 16;
};
SamplerState PointRepeatSampler_id49 
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState LinearRepeatSampler_id50 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState RepeatSampler_id51 
{
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState Sampler0_id52;
SamplerState Sampler1_id53;
SamplerState Sampler2_id54;
SamplerState Sampler3_id55;
SamplerState Sampler4_id56;
SamplerState Sampler5_id57;
SamplerState Sampler6_id58;
SamplerState Sampler7_id59;
SamplerState Sampler8_id60;
SamplerState Sampler9_id61;
float4 Shading_id3(inout PS_STREAMS streams)
{
    return Texture0_id14.Sample(Sampler_id42, streams.TexCoord_id62);
}
float4 Shading_id4(inout PS_STREAMS streams)
{
    float4 swizzleColor = (abs(streams.Swizzle_id77 - 1) <= 0.1) ? Shading_id3(streams).rrrr : Shading_id3(streams);
    if (abs(streams.Swizzle_id77 - 2) <= 0.1)
    {
        float nX = swizzleColor.r * 2 - 1;
        float nY = swizzleColor.g * 2 - 1;
        swizzleColor.a = 1;
        float nZ = 1 - sqrt(saturate(nX * nX + nY * nY));
        swizzleColor.b = nZ * 0.5f + 0.5f;
    }
    if (abs(streams.Swizzle_id77 - 3) <= 0.1)
    {
        swizzleColor.gb = swizzleColor.rr;
        swizzleColor.a = 1;
    }
    float4 finalColor = swizzleColor * streams.Color_id75 + streams.ColorAdd_id76;
    clip(finalColor.a - 0.1);
    return finalColor;
}
float4 ToLinear_id5(float4 sRGBa)
{
    float3 sRGB = sRGBa.rgb;
    return float4(sRGB * (sRGB * (sRGB * 0.305306011 + 0.682171111) + 0.012522878), sRGBa.a);
}
void VSMain_id2(inout VS_STREAMS streams)
{
    streams.ShadingPosition_id0 = mul(streams.Position_id72, MatrixTransform_id73);
}
PS_OUTPUT PSMain(PS_INPUT __input__)
{
    PS_STREAMS streams = (PS_STREAMS)0;
    streams.Color_id75 = __input__.Color_id75;
    streams.Swizzle_id77 = __input__.Swizzle_id77;
    streams.TexCoord_id62 = __input__.TexCoord_id62;
    streams.ColorAdd_id76 = __input__.ColorAdd_id76;
    streams.ColorTarget_id2 = Shading_id4(streams);
    PS_OUTPUT __output__ = (PS_OUTPUT)0;
    __output__.ColorTarget_id2 = streams.ColorTarget_id2;
    return __output__;
}
VS_OUTPUT VSMain(VS_INPUT __input__)
{
    VS_STREAMS streams = (VS_STREAMS)0;
    streams.Position_id72 = __input__.Position_id72;
    streams.Color_id75 = __input__.Color_id75;
    streams.Swizzle_id77 = __input__.Swizzle_id77;
    streams.TexCoord_id62 = __input__.TexCoord_id62;
    streams.ColorAdd_id76 = __input__.ColorAdd_id76;
    VSMain_id2(streams);
    VS_OUTPUT __output__ = (VS_OUTPUT)0;
    __output__.ShadingPosition_id0 = streams.ShadingPosition_id0;
    __output__.Color_id75 = streams.Color_id75;
    __output__.Swizzle_id77 = streams.Swizzle_id77;
    __output__.TexCoord_id62 = streams.TexCoord_id62;
    __output__.ColorAdd_id76 = streams.ColorAdd_id76;
    return __output__;
}
