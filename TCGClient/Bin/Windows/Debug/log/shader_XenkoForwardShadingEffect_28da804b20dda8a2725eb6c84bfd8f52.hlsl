/**************************
***** Compiler Parameters *****
***************************
@P EffectName: XenkoForwardShadingEffect
@P   - Material.PixelStageSurfaceShaders: mixin MaterialSurfaceArray [{layers = [mixin MaterialSurfaceDiffuse [{diffuseMap = ComputeColorConstantColorLink<Material.DiffuseValue>}], mixin MaterialSurfaceStreamsBlend [{blends = [MaterialStreamLinearBlend<matDiffuse>, MaterialStreamLinearBlend<matColorBase>]}, {layer = mixin MaterialSurfaceArray [{layers = [mixin MaterialSurfaceDiffuse [{diffuseMap = ComputeColorTextureScaledOffsetDynamicSampler<Material.DiffuseMap,TEXCOORD0,Material.Sampler.i0,rgba,Material.TextureScale,Material.TextureOffset>}], mixin MaterialSurfaceSetStreamFromComputeColor<matBlend,r> [{computeColorSource = mixin ComputeColorMultiply [{color1 = ComputeColorConstantFloatLink<Material.BlendValue>}, {color2 = ComputeColorTextureScaledOffsetDynamicSampler<Material.BlendMap,TEXCOORD0,Material.Sampler.i0,r,Material.TextureScale.i1,Material.TextureOffset.i1>}]}]]}]}], mixin MaterialSurfaceLightingAndShading [{surfaces = [MaterialSurfaceShadingDiffuseLambert<false>]}]]}]
@P Material.PixelStageStreamInitializer: mixin MaterialStream, MaterialPixelShadingStream
@P Lighting.DirectLightGroups: LightClusteredPointGroup, LightClusteredSpotGroup
@P Lighting.EnvironmentLights: LightSimpleAmbient, EnvironmentLight
@P XenkoEffectBase.RenderTargetExtensions: mixin
***************************
****  ConstantBuffers  ****
***************************
cbuffer PerDraw [Size: 416]
@C    World_id31 => Transformation.World
@C    WorldInverse_id32 => Transformation.WorldInverse
@C    WorldInverseTranspose_id33 => Transformation.WorldInverseTranspose
@C    WorldView_id34 => Transformation.WorldView
@C    WorldViewInverse_id35 => Transformation.WorldViewInverse
@C    WorldViewProjection_id36 => Transformation.WorldViewProjection
@C    WorldScale_id37 => Transformation.WorldScale
@C    EyeMS_id38 => Transformation.EyeMS
cbuffer PerMaterial [Size: 64]
@C    constantColor_id99 => Material.DiffuseValue
@C    scale_id105 => Material.TextureScale
@C    offset_id106 => Material.TextureOffset
@C    constantFloat_id168 => Material.BlendValue
@C    scale_id171 => Material.TextureScale.i1
@C    offset_id172 => Material.TextureOffset.i1
cbuffer PerView [Size: 448]
@C    View_id24 => Transformation.View
@C    ViewInverse_id25 => Transformation.ViewInverse
@C    Projection_id26 => Transformation.Projection
@C    ProjectionInverse_id27 => Transformation.ProjectionInverse
@C    ViewProjection_id28 => Transformation.ViewProjection
@C    ProjScreenRay_id29 => Transformation.ProjScreenRay
@C    Eye_id30 => Transformation.Eye
@C    NearClipPlane_id84 => Camera.NearClipPlane
@C    FarClipPlane_id85 => Camera.FarClipPlane
@C    ZProjection_id86 => Camera.ZProjection
@C    ViewSize_id87 => Camera.ViewSize
@C    AspectRatio_id88 => Camera.AspectRatio
@C    _padding_PerView_Default => _padding_PerView_Default
@C    ClusterDepthScale_id93 => LightClustered.ClusterDepthScale
@C    ClusterDepthBias_id94 => LightClustered.ClusterDepthBias
@C    ClusterStride_id95 => LightClustered.ClusterStride
@C    AmbientLight_id98 => LightSimpleAmbient.AmbientLight.environmentLights[0]
@C    _padding_PerView_Lighting => _padding_PerView_Lighting
***************************
******  Resources    ******
***************************
@R    PerMaterial => PerMaterial [Stage: None, Slot: (-1--1)]
@R    Texture_id102 => Material.DiffuseMap [Stage: None, Slot: (-1--1)]
@R    Texture_id102 => Material.DiffuseMap [Stage: None, Slot: (-1--1)]
@R    Sampler_id103 => Material.Sampler.i0 [Stage: None, Slot: (-1--1)]
@R    Sampler_id103 => Material.Sampler.i0 [Stage: None, Slot: (-1--1)]
@R    Texture_id169 => Material.BlendMap [Stage: None, Slot: (-1--1)]
@R    Texture_id169 => Material.BlendMap [Stage: None, Slot: (-1--1)]
@R    LightClusters_id91 => LightClustered.LightClusters [Stage: None, Slot: (-1--1)]
@R    LightClusters_id91 => LightClustered.LightClusters [Stage: None, Slot: (-1--1)]
@R    LightIndices_id92 => LightClustered.LightIndices [Stage: None, Slot: (-1--1)]
@R    LightIndices_id92 => LightClustered.LightIndices [Stage: None, Slot: (-1--1)]
@R    PointLights_id96 => LightClusteredPointGroup.PointLights [Stage: None, Slot: (-1--1)]
@R    PointLights_id96 => LightClusteredPointGroup.PointLights [Stage: None, Slot: (-1--1)]
@R    SpotLights_id97 => LightClusteredSpotGroup.SpotLights [Stage: None, Slot: (-1--1)]
@R    SpotLights_id97 => LightClusteredSpotGroup.SpotLights [Stage: None, Slot: (-1--1)]
@R    PerDraw => PerDraw [Stage: Vertex, Slot: (0-0)]
@R    PerView => PerView [Stage: Vertex, Slot: (1-1)]
@R    Sampler_id103 => Material.Sampler.i0 [Stage: Pixel, Slot: (0-0)]
@R    Texture_id102 => Material.DiffuseMap [Stage: Pixel, Slot: (0-0)]
@R    Texture_id169 => Material.BlendMap [Stage: Pixel, Slot: (1-1)]
@R    LightClusters_id91 => LightClustered.LightClusters [Stage: Pixel, Slot: (2-2)]
@R    LightIndices_id92 => LightClustered.LightIndices [Stage: Pixel, Slot: (3-3)]
@R    PointLights_id96 => LightClusteredPointGroup.PointLights [Stage: Pixel, Slot: (4-4)]
@R    SpotLights_id97 => LightClusteredSpotGroup.SpotLights [Stage: Pixel, Slot: (5-5)]
@R    PerMaterial => PerMaterial [Stage: Pixel, Slot: (0-0)]
@R    PerView => PerView [Stage: Pixel, Slot: (1-1)]
***************************
*****     Sources     *****
***************************
@S    ShaderBase => acbe3d4d44a046eede871176bee9c754
@S    ShaderBaseStream => a3a5bf8185f2a3d89972293f806430d3
@S    ShadingBase => a56c21640e78c756e8d56651480eb9f5
@S    ComputeColor => ded06879812e042b84d284d2272e4b4a
@S    TransformationBase => be8628f6067b518dd5c7b3fe338b9320
@S    NormalStream => b59c6ee174b93be981cc113a7a70d70b
@S    TransformationWAndVP => 37eaa3c16c9e83fd77e04c47c6794803
@S    PositionStream4 => b1f2243b30eb87e6e40bc6af56b4fd18
@S    PositionHStream4 => 0c0bb9059a8e3199d9ec950f3a7d2b66
@S    Transformation => ce8c4a6980d1f949f5f9aed15679c96d
@S    NormalFromMesh => 7e93d74a2c2c59456ebca897ee2c4bdb
@S    NormalBase => 118edc0075e4c3a87c3cb4570a808039
@S    NormalUpdate => 9bf3758fe45b1105750554fed463a15e
@S    MaterialSurfacePixelStageCompositor => cb7b21ca902e1b289c9d765318058f5a
@S    PositionStream => d767885dace5d697c6532bb4f0f5b3f8
@S    MaterialPixelShadingStream => ce423644e4da2371ef9e6b451c39edf7
@S    MaterialPixelStream => 83c4d63f50d89eb133457f26bcc25822
@S    MaterialStream => f4c30b25d4f10a4a3809b97598bfca17
@S    IStreamInitializer => 37d0ab08af9e2896a54aa43c0ea2ad0f
@S    LightStream => 622a7adc4e53980a3d0e0fecffc76661
@S    DirectLightGroupArray => b6017f14fd58343b7a54fefa2ca81610
@S    DirectLightGroup => 08ae1198dfb5788ff8584d747c3b8368
@S    ShadowGroup => 05cfb339de033879838a6606f047a866
@S    ShadowStream => 38650f3182fa7d3cab3bae43f9398f4e
@S    TextureProjectionGroup => 47f8334211d8d5ae6d98f32a923759cc
@S    EnvironmentLightArray => 4129555ea2051a98f5cf5e315d791d3e
@S    EnvironmentLight => 6aceaad54f2057382904d7f6dfa58e4f
@S    IMaterialSurface => d56637e1116951bd72b1817daa1a6158
@S    LightClusteredPointGroup => c5703508c16a19f7acb9ebe6676eeb4a
@S    LightClustered => 82f6bcf6878ecb8f1cf19364969848f1
@S    ScreenPositionBase => b66b925e44d5cef758dd09d560d88680
@S    Camera => 38d8cc7176ac62fc4d9c97d70f9013ab
@S    LightPoint => 5cc6bc739ab0f656d8a48332e8ca569b
@S    LightUtil => 2587d00dc291bbace485f1420ae464ad
@S    LightClusteredSpotGroup => d9e6bd2fe470894c4d8bebba13305bf2
@S    LightSpot => ff13b9737bc99d2458fa5aa679b6b8f0
@S    SpotLightDataInternalShader => 4c9afecddd31668dbea2f90869a2d5f0
@S    LightSpotAttenuationDefault => a424c87e1fb86625b52e4ab0ab01bf46
@S    LightSimpleAmbient => 5216c2bc901f0543bc29393671a09451
@S    MaterialSurfaceArray => ca7a8b492198ae093d4f490c2ba6aaec
@S    MaterialSurfaceDiffuse => c60d7ca8058e062fcba77cc9b4c7d496
@S    IMaterialSurfacePixel => b5f583c7b871b6a4ebe5c7411883503e
@S    ComputeColorConstantColorLink => a30d6cd76f7c0875cdeffdb3ae8bae33
@S    MaterialSurfaceStreamsBlend => 7b9a5e020df0b50c27bfbaf7048da29c
@S    IMaterialStreamBlend => 03a3ab664cbfb7287b86d8e110b7198b
@S    MaterialVertexStream => 88c18bd25810bbb05cdd7c08ebc8c4a2
@S    MaterialDisplacementStream => 2f1b0f776096589363e85354152ef62e
@S    MaterialStreamLinearBlend => 4068c07fcf8d71af41a1594d6a9ada1f
@S    ComputeColorTextureScaledOffsetDynamicSampler => 680e61052fd580928ba07d61381cf7aa
@S    DynamicTexture => c9a2e65d8e419b069971342ac1eaffe1
@S    DynamicSampler => c7e36b002252e5b947f67278b28b0957
@S    DynamicTextureStream => 49c11eb479810dd1fa94c1639e04e5cf
@S    MaterialSurfaceSetStreamFromComputeColor => c2d77b2aa35d9f24b852eb4a5fdc9872
@S    IMaterialSurfaceVertex => 0c5c73d45a721c87c347ae02954665ce
@S    IMaterialSurfaceDomain => f21c77ffcac748537e778154fecb0921
@S    MaterialDomainStream => cf19dcc996d2d1de16031ccad6eafac0
@S    MaterialTessellationStream => fc291f489bf9de3c2dd85f2960efacc9
@S    Texturing => e6daef0dd90a55f9549c6f5d291e61a5
@S    ComputeColorMultiply => e109055ae3be65656da4c4d78b0585fb
@S    ComputeColorConstantFloatLink => 12410f8b57e4015ff1a3fce241440b62
@S    MaterialSurfaceLightingAndShading => 68ac8c9f25624fdc15ea262fbc868030
@S    Math => 49d7f7706890095f248caeaf232f4db4
@S    IMaterialSurfaceShading => 43b5938a14c30cfc19b2ddcb76824cbe
@S    MaterialSurfaceShadingDiffuseLambert => ebe9647a0e11903ef16a793ffdbdd34c
***************************
*****     Stages      *****
***************************
@G    Vertex => 1fdff4d58aa42c0513b5ccc8afd21604
//
// Generated by Microsoft (R) HLSL Shader Compiler 10.1
//
//
// Buffer Definitions: 
//
// cbuffer PerDraw
// {
//
//   float4x4 World_id31;               // Offset:    0 Size:    64
//   float4x4 WorldInverse_id32;        // Offset:   64 Size:    64 [unused]
//   float4x4 WorldInverseTranspose_id33;// Offset:  128 Size:    64
//   float4x4 WorldView_id34;           // Offset:  192 Size:    64 [unused]
//   float4x4 WorldViewInverse_id35;    // Offset:  256 Size:    64 [unused]
//   float4x4 WorldViewProjection_id36; // Offset:  320 Size:    64 [unused]
//   float3 WorldScale_id37;            // Offset:  384 Size:    12 [unused]
//   float4 EyeMS_id38;                 // Offset:  400 Size:    16 [unused]
//
// }
//
// cbuffer PerView
// {
//
//   float4x4 View_id24;                // Offset:    0 Size:    64 [unused]
//   float4x4 ViewInverse_id25;         // Offset:   64 Size:    64 [unused]
//   float4x4 Projection_id26;          // Offset:  128 Size:    64 [unused]
//   float4x4 ProjectionInverse_id27;   // Offset:  192 Size:    64 [unused]
//   float4x4 ViewProjection_id28;      // Offset:  256 Size:    64
//   float2 ProjScreenRay_id29;         // Offset:  320 Size:     8 [unused]
//   float4 Eye_id30;                   // Offset:  336 Size:    16 [unused]
//   float NearClipPlane_id84;          // Offset:  352 Size:     4 [unused]
//      = 0x3f800000 
//   float FarClipPlane_id85;           // Offset:  356 Size:     4 [unused]
//      = 0x42c80000 
//   float2 ZProjection_id86;           // Offset:  360 Size:     8 [unused]
//   float2 ViewSize_id87;              // Offset:  368 Size:     8 [unused]
//   float AspectRatio_id88;            // Offset:  376 Size:     4 [unused]
//   float4 _padding_PerView_Default;   // Offset:  384 Size:    16 [unused]
//   float ClusterDepthScale_id93;      // Offset:  400 Size:     4 [unused]
//   float ClusterDepthBias_id94;       // Offset:  404 Size:     4 [unused]
//   float2 ClusterStride_id95;         // Offset:  408 Size:     8 [unused]
//   float3 AmbientLight_id98;          // Offset:  416 Size:    12 [unused]
//   float4 _padding_PerView_Lighting;  // Offset:  432 Size:    16 [unused]
//
// }
//
//
// Resource Bindings:
//
// Name                                 Type  Format         Dim      HLSL Bind  Count
// ------------------------------ ---------- ------- ----------- -------------- ------
// PerDraw                           cbuffer      NA          NA            cb0      1 
// PerView                           cbuffer      NA          NA            cb1      1 
//
//
//
// Input signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// POSITION                 0   xyzw        0     NONE   float   xyzw
// NORMAL                   0   xyz         1     NONE   float   xyz 
// TEXCOORD                 0   xy          2     NONE   float   xy  
//
//
// Output signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// POSITION_WS              0   xyzw        0     NONE   float   xyzw
// SV_Position              0   xyzw        1      POS   float   xyzw
// NORMALWS                 0   xyz         2     NONE   float   xyz 
// SCREENPOSITION_ID83_SEM     0   xyzw        3     NONE   float   xyzw
// TEXCOORD                 0   xy          4     NONE   float   xy  
//
vs_4_0
dcl_constantbuffer CB0[11], immediateIndexed
dcl_constantbuffer CB1[20], immediateIndexed
dcl_input v0.xyzw
dcl_input v1.xyz
dcl_input v2.xy
dcl_output o0.xyzw
dcl_output_siv o1.xyzw, position
dcl_output o2.xyz
dcl_output o3.xyzw
dcl_output o4.xy
dcl_temps 2
//
// Initial variable locations:
//   v0.x <- __input__.Position_id20.x; v0.y <- __input__.Position_id20.y; v0.z <- __input__.Position_id20.z; v0.w <- __input__.Position_id20.w; 
//   v1.x <- __input__.meshNormal_id15.x; v1.y <- __input__.meshNormal_id15.y; v1.z <- __input__.meshNormal_id15.z; 
//   v2.x <- __input__.TexCoord_id104.x; v2.y <- __input__.TexCoord_id104.y; 
//   o4.x <- <VSMain return value>.TexCoord_id104.x; o4.y <- <VSMain return value>.TexCoord_id104.y; 
//   o3.x <- <VSMain return value>.ScreenPosition_id83.x; o3.y <- <VSMain return value>.ScreenPosition_id83.y; o3.z <- <VSMain return value>.ScreenPosition_id83.z; o3.w <- <VSMain return value>.ScreenPosition_id83.w; 
//   o2.x <- <VSMain return value>.normalWS_id18.x; o2.y <- <VSMain return value>.normalWS_id18.y; o2.z <- <VSMain return value>.normalWS_id18.z; 
//   o1.x <- <VSMain return value>.ShadingPosition_id0.x; o1.y <- <VSMain return value>.ShadingPosition_id0.y; o1.z <- <VSMain return value>.ShadingPosition_id0.z; o1.w <- <VSMain return value>.ShadingPosition_id0.w; 
//   o0.x <- <VSMain return value>.PositionWS_id21.x; o0.y <- <VSMain return value>.PositionWS_id21.y; o0.z <- <VSMain return value>.PositionWS_id21.z; o0.w <- <VSMain return value>.PositionWS_id21.w
//
#line 723 "C:\Users\minor\Documents\Xenko Projects\TCGClient\TCGClient\Bin\Windows\Debug\log\shader_XenkoForwardShadingEffect_28da804b20dda8a2725eb6c84bfd8f52.hlsl"
dp4 r0.x, v0.xyzw, cb0[0].xyzw  // r0.x <- streams.PositionWS_id21.x
dp4 r0.y, v0.xyzw, cb0[1].xyzw  // r0.y <- streams.PositionWS_id21.y
dp4 r0.z, v0.xyzw, cb0[2].xyzw  // r0.z <- streams.PositionWS_id21.z
dp4 r0.w, v0.xyzw, cb0[3].xyzw  // r0.w <- streams.PositionWS_id21.w

#line 805
mov o0.xyzw, r0.xyzw

#line 682
dp4 r1.x, r0.xyzw, cb1[16].xyzw  // r1.x <- <ComputeShadingPosition_id11 return value>.x
dp4 r1.y, r0.xyzw, cb1[17].xyzw  // r1.y <- <ComputeShadingPosition_id11 return value>.y
dp4 r1.z, r0.xyzw, cb1[18].xyzw  // r1.z <- <ComputeShadingPosition_id11 return value>.z
dp4 r1.w, r0.xyzw, cb1[19].xyzw  // r1.w <- <ComputeShadingPosition_id11 return value>.w

#line 805
mov o1.xyzw, r1.xyzw
mov o3.xyzw, r1.xyzw

#line 758
dp3 o2.x, v1.xyzx, cb0[8].xyzx
dp3 o2.y, v1.xyzx, cb0[9].xyzx
dp3 o2.z, v1.xyzx, cb0[10].xyzx

#line 805
mov o4.xy, v2.xyxx
ret 
// Approximately 16 instruction slots used
@G    Pixel => 2f633d4b64d2bf24ea5cb7a9748ccfb5
//
// Generated by Microsoft (R) HLSL Shader Compiler 10.1
//
//
// Buffer Definitions: 
//
// cbuffer PerMaterial
// {
//
//   float4 constantColor_id99;         // Offset:    0 Size:    16
//   float2 scale_id105;                // Offset:   16 Size:     8
//   float2 offset_id106;               // Offset:   24 Size:     8
//   float constantFloat_id168;         // Offset:   32 Size:     4
//   float2 scale_id171;                // Offset:   36 Size:     8
//   float2 offset_id172;               // Offset:   48 Size:     8
//
// }
//
// cbuffer PerView
// {
//
//   float4x4 View_id24;                // Offset:    0 Size:    64 [unused]
//   float4x4 ViewInverse_id25;         // Offset:   64 Size:    64 [unused]
//   float4x4 Projection_id26;          // Offset:  128 Size:    64 [unused]
//   float4x4 ProjectionInverse_id27;   // Offset:  192 Size:    64 [unused]
//   float4x4 ViewProjection_id28;      // Offset:  256 Size:    64 [unused]
//   float2 ProjScreenRay_id29;         // Offset:  320 Size:     8 [unused]
//   float4 Eye_id30;                   // Offset:  336 Size:    16 [unused]
//   float NearClipPlane_id84;          // Offset:  352 Size:     4 [unused]
//      = 0x3f800000 
//   float FarClipPlane_id85;           // Offset:  356 Size:     4 [unused]
//      = 0x42c80000 
//   float2 ZProjection_id86;           // Offset:  360 Size:     8
//   float2 ViewSize_id87;              // Offset:  368 Size:     8 [unused]
//   float AspectRatio_id88;            // Offset:  376 Size:     4 [unused]
//   float4 _padding_PerView_Default;   // Offset:  384 Size:    16 [unused]
//   float ClusterDepthScale_id93;      // Offset:  400 Size:     4
//   float ClusterDepthBias_id94;       // Offset:  404 Size:     4
//   float2 ClusterStride_id95;         // Offset:  408 Size:     8
//   float3 AmbientLight_id98;          // Offset:  416 Size:    12
//   float4 _padding_PerView_Lighting;  // Offset:  432 Size:    16 [unused]
//
// }
//
//
// Resource Bindings:
//
// Name                                 Type  Format         Dim      HLSL Bind  Count
// ------------------------------ ---------- ------- ----------- -------------- ------
// Sampler_id103                     sampler      NA          NA             s0      1 
// Texture_id102                     texture  float4          2d             t0      1 
// Texture_id169                     texture  float4          2d             t1      1 
// LightClusters_id91                texture   uint2          3d             t2      1 
// LightIndices_id92                 texture    uint         buf             t3      1 
// PointLights_id96                  texture  float4         buf             t4      1 
// SpotLights_id97                   texture  float4         buf             t5      1 
// PerMaterial                       cbuffer      NA          NA            cb0      1 
// PerView                           cbuffer      NA          NA            cb1      1 
//
//
//
// Input signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// POSITION_WS              0   xyzw        0     NONE   float   xyz 
// SV_Position              0   xyzw        1      POS   float     z 
// NORMALWS                 0   xyz         2     NONE   float   xyz 
// SCREENPOSITION_ID83_SEM     0   xyzw        3     NONE   float   xy w
// TEXCOORD                 0   xy          4     NONE   float   xy  
// SV_IsFrontFace           0   x           5    FFACE    uint   x   
//
//
// Output signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// SV_Target                0   xyzw        0   TARGET   float   xyzw
//
ps_4_0
dcl_constantbuffer CB0[4], immediateIndexed
dcl_constantbuffer CB1[27], immediateIndexed
dcl_sampler s0, mode_default
dcl_resource_texture2d (float,float,float,float) t0
dcl_resource_texture2d (float,float,float,float) t1
dcl_resource_texture3d (uint,uint,uint,uint) t2
dcl_resource_buffer (uint,uint,uint,uint) t3
dcl_resource_buffer (float,float,float,float) t4
dcl_resource_buffer (float,float,float,float) t5
dcl_input_ps linear v0.xyz
dcl_input_ps_siv linear noperspective v1.z, position
dcl_input_ps linear v2.xyz
dcl_input_ps linear v3.xyw
dcl_input_ps linear v4.xy
dcl_input_ps_sgv constant v5.x, is_front_face
dcl_output o0.xyzw
dcl_temps 12
//
// Initial variable locations:
//   v0.x <- __input__.PositionWS_id21.x; v0.y <- __input__.PositionWS_id21.y; v0.z <- __input__.PositionWS_id21.z; v0.w <- __input__.PositionWS_id21.w; 
//   v1.x <- __input__.ShadingPosition_id0.x; v1.y <- __input__.ShadingPosition_id0.y; v1.z <- __input__.ShadingPosition_id0.z; v1.w <- __input__.ShadingPosition_id0.w; 
//   v2.x <- __input__.normalWS_id18.x; v2.y <- __input__.normalWS_id18.y; v2.z <- __input__.normalWS_id18.z; 
//   v3.x <- __input__.ScreenPosition_id83.x; v3.y <- __input__.ScreenPosition_id83.y; v3.z <- __input__.ScreenPosition_id83.z; v3.w <- __input__.ScreenPosition_id83.w; 
//   v4.x <- __input__.TexCoord_id104.x; v4.y <- __input__.TexCoord_id104.y; 
//   v5.x <- __input__.IsFrontFace_id1; 
//   o0.x <- <PSMain return value>.ColorTarget_id2.x; o0.y <- <PSMain return value>.ColorTarget_id2.y; o0.z <- <PSMain return value>.ColorTarget_id2.z; o0.w <- <PSMain return value>.ColorTarget_id2.w
//
#line 785 "C:\Users\minor\Documents\Xenko Projects\TCGClient\TCGClient\Bin\Windows\Debug\log\shader_XenkoForwardShadingEffect_28da804b20dda8a2725eb6c84bfd8f52.hlsl"
div r0.xy, v3.xyxx, v3.wwww  // r0.x <- streams.ScreenPosition_id83.x; r0.y <- streams.ScreenPosition_id83.y

#line 752
dp3 r0.z, v2.xyzx, v2.xyzx
lt r0.w, l(0.000000), r0.z

#line 753
rsq r0.z, r0.z
mul r1.xyz, r0.zzzz, v2.xyzx  // r1.x <- streams.normalWS_id18.x; r1.y <- streams.normalWS_id18.y; r1.z <- streams.normalWS_id18.z
movc r1.xyz, r0.wwww, r1.xyzx, v2.xyzx

#line 347
mad r0.zw, v4.xxxy, cb0[1].xxxy, cb0[1].zzzw
sample r2.xyzw, r0.zwzz, t0.xyzw, s0  // r2.x <- <Compute_id144 return value>.x; r2.y <- <Compute_id144 return value>.y; r2.z <- <Compute_id144 return value>.z; r2.w <- <Compute_id144 return value>.w

#line 317
mad r0.zw, v4.xxxy, cb0[2].yyyz, cb0[3].xxxy
sample r3.xyzw, r0.zwzz, t1.xyzw, s0  // r3.x <- <Compute_id148 return value>.x

#line 342
mul r0.z, r3.x, cb0[2].x  // r0.z <- mix1.x

#line 539
add r2.xyzw, r2.xyzw, -cb0[0].xyzw
mad r2.xyzw, r0.zzzz, r2.xyzw, cb0[0].xyzw  // r2.x <- streams.matDiffuse_id51.x; r2.y <- streams.matDiffuse_id51.y; r2.z <- streams.matDiffuse_id51.z; r2.w <- streams.matDiffuse_id51.w

#line 565
movc r1.xyz, v5.xxxx, r1.xyzx, -r1.xyzx  // r1.x <- streams.normalWS_id18.x; r1.y <- streams.normalWS_id18.y; r1.z <- streams.normalWS_id18.z

#line 410
add r0.z, v1.z, -cb1[22].z
div r0.z, cb1[22].w, r0.z  // r0.z <- depth

#line 411
mad r0.xy, r0.xyxx, l(1.000000, -1.000000, 0.000000, 0.000000), l(1.000000, 1.000000, 0.000000, 0.000000)
mul r0.xy, r0.xyxx, cb1[25].zwzz

#line 412
mad r0.z, r0.z, cb1[25].x, cb1[25].y
log r0.z, r0.z
max r0.z, r0.z, l(0.000000)

#line 413
mul r0.xy, r0.xyxx, l(0.500000, 0.500000, 0.000000, 0.000000)

#line 412
ftoi r3.xyz, r0.xyzx  // r3.z <- slice

#line 413
mov r3.w, l(0)
ld r0.xyzw, r3.xyzw, t2.xyzw  // r0.x <- streams.lightData_id89.x; r0.y <- streams.lightData_id89.y

#line 500
and r0.z, r0.y, l(0x0000ffff)  // r0.z <- <GetMaxLightCount_id110 return value>

#line 579
mov r3.xyz, r1.xyzx  // r3.x <- streams.normalWS_id18.x; r3.y <- streams.normalWS_id18.y; r3.z <- streams.normalWS_id18.z
mov r3.w, v0.x  // r3.w <- streams.PositionWS_id21.x
mov r4.yz, v0.yyzy  // r4.y <- streams.PositionWS_id21.y; r4.z <- streams.PositionWS_id21.z
mov r5.xyz, r2.xyzx  // r5.x <- streams.matDiffuseVisible_id67.x; r5.y <- streams.matDiffuseVisible_id67.y; r5.z <- streams.matDiffuseVisible_id67.z
mov r6.xyz, l(0,0,0,0)  // r6.x <- directLightingContribution.x; r6.y <- directLightingContribution.y; r6.z <- directLightingContribution.z
mov r7.x, r0.x  // r7.x <- streams.lightIndex_id90
mov r0.w, l(0)  // r0.w <- i
loop 
  ige r1.w, r0.w, r0.z
  breakc_nz r1.w

#line 581
  if_nz r1.w

#line 583
    break 

#line 584
  endif 

#line 397
  ld r8.xyzw, r7.xxxx, t3.xyzw  // r8.x <- realLightIndex

#line 398
  iadd r7.x, r7.x, l(1)  // r7.x <- streams.lightIndex_id90

#line 400
  ishl r1.w, r8.x, l(1)
  ld r8.xyzw, r1.wwww, t4.xyzw  // r8.x <- pointLight1.x; r8.y <- pointLight1.y; r8.z <- pointLight1.z; r8.w <- pointLight1.w

#line 401
  iadd r1.w, r1.w, l(1)
  ld r9.xyzw, r1.wwww, t4.xyzw  // r9.x <- pointLight2.x; r9.y <- pointLight2.y; r9.z <- pointLight2.z

#line 309
  mov r4.x, r3.w
  add r7.yzw, -r4.xxyz, r8.xxyz  // r7.y <- lightVector.x; r7.z <- lightVector.y; r7.w <- lightVector.z

#line 310
  dp3 r1.w, r7.yzwy, r7.yzwy
  sqrt r4.x, r1.w  // r4.x <- lightVectorLength

#line 311
  div r7.yzw, r7.yyzw, r4.xxxx  // r7.y <- lightVectorNorm.x; r7.z <- lightVectorNorm.y; r7.w <- lightVectorNorm.z

#line 290
  max r4.x, r1.w, l(0.000100)
  div r4.x, l(1.000000, 1.000000, 1.000000, 1.000000), r4.x  // r4.x <- attenuation

#line 269
  mul r1.w, r8.w, r1.w  // r1.w <- factor

#line 270
  mad r1.w, -r1.w, r1.w, l(1.000000)
  max r1.w, r1.w, l(0.000000)  // r1.w <- smoothFactor

#line 271
  mul r1.w, r1.w, r1.w  // r1.w <- <SmoothDistanceAttenuation_id48 return value>

#line 291
  mul r1.w, r1.w, r4.x  // r1.w <- attenuation

#line 335
  mul r8.xyz, r1.wwww, r9.xyzx  // r8.x <- streams.lightColor_id42.x; r8.y <- streams.lightColor_id42.y; r8.z <- streams.lightColor_id42.z

#line 486
  dp3 r1.w, r3.xyzx, r7.yzwy
  max r1.w, r1.w, l(0.000100)  // r1.w <- streams.NdotL_id47

#line 488
  mul r7.yzw, r1.wwww, r8.xxyz  // r7.y <- streams.lightColorNdotL_id43.x; r7.z <- streams.lightColorNdotL_id43.y; r7.w <- streams.lightColorNdotL_id43.z

#line 474
  mul r7.yzw, r7.yyzw, r5.xxyz

#line 589
  mad r6.xyz, r7.yzwy, l(0.318310, 0.318310, 0.318310, 0.000000), r6.xyzx

#line 579
  iadd r0.w, r0.w, l(1)

#line 591
endloop   // r7.x <- streams.lightIndex_id90

#line 466
ushr r0.x, r0.y, l(16)  // r0.x <- <GetMaxLightCount_id122 return value>

#line 599
mov r3.xyz, r1.xyzx
mov r3.w, v0.x
mov r4.yz, v0.yyzy
mov r0.yzw, r2.xxyz  // r0.y <- streams.matDiffuseVisible_id67.x; r0.z <- streams.matDiffuseVisible_id67.y; r0.w <- streams.matDiffuseVisible_id67.z
mov r5.xyz, r6.xyzx  // r5.x <- directLightingContribution.x; r5.y <- directLightingContribution.y; r5.z <- directLightingContribution.z
mov r1.w, r7.x  // r1.w <- streams.lightIndex_id90
mov r4.w, l(0)  // r4.w <- i
loop 
  ige r5.w, r4.w, r0.x
  breakc_nz r5.w

#line 601
  if_nz r5.w

#line 603
    break 

#line 604
  endif 

#line 369
  ld r8.xyzw, r1.wwww, t3.xyzw  // r8.x <- realLightIndex

#line 370
  iadd r1.w, r1.w, l(1)  // r1.w <- streams.lightIndex_id90

#line 372
  ishl r5.w, r8.x, l(2)
  ld r8.xyzw, r5.wwww, t5.xyzw  // r8.x <- spotLight1.x; r8.y <- spotLight1.y; r8.z <- spotLight1.z

#line 375
  iadd r7.yzw, r5.wwww, l(0, 1, 2, 3)

#line 373
  ld r9.xyzw, r7.yyyy, t5.xyzw  // r9.x <- spotLight2.x; r9.y <- spotLight2.y; r9.z <- spotLight2.z

#line 374
  ld r10.xyzw, r7.zzzz, t5.xyzw  // r10.x <- spotLight3.x; r10.y <- spotLight3.y; r10.z <- spotLight3.z

#line 375
  ld r11.xyzw, r7.wwww, t5.xyzw  // r11.x <- spotLight4.x; r11.y <- spotLight4.y; r11.z <- spotLight4.z

#line 296
  mov r4.x, r3.w
  add r7.yzw, -r4.xxyz, r8.xxyz  // r7.y <- lightVector.x; r7.z <- lightVector.y; r7.w <- lightVector.z

#line 297
  dp3 r4.x, r7.yzwy, r7.yzwy
  sqrt r5.w, r4.x  // r5.w <- lightVectorLength

#line 298
  div r7.yzw, r7.yyzw, r5.wwww  // r7.y <- lightVectorNorm.x; r7.z <- lightVectorNorm.y; r7.w <- lightVectorNorm.z

#line 283
  max r5.w, r4.x, l(0.000100)
  div r5.w, l(1.000000, 1.000000, 1.000000, 1.000000), r5.w  // r5.w <- attenuation

#line 263
  mul r4.x, r10.z, r4.x  // r4.x <- factor

#line 264
  mad r4.x, -r4.x, r4.x, l(1.000000)
  max r4.x, r4.x, l(0.000000)  // r4.x <- smoothFactor

#line 265
  mul r4.x, r4.x, r4.x  // r4.x <- <SmoothDistanceAttenuation_id59 return value>

#line 284
  mul r4.x, r4.x, r5.w  // r4.x <- attenuation

#line 275
  dp3 r5.w, -r9.xyzx, r7.yzwy  // r5.w <- cd

#line 276
  mad_sat r5.w, r5.w, r10.x, r10.y  // r5.w <- attenuation

#line 277
  mul r5.w, r5.w, r5.w

#line 304
  mul r4.x, r4.x, r5.w  // r4.x <- attenuation

#line 327
  mul r8.xyz, r4.xxxx, r11.xyzx  // r8.x <- streams.lightColor_id42.x; r8.y <- streams.lightColor_id42.y; r8.z <- streams.lightColor_id42.z

#line 452
  dp3 r4.x, r3.xyzx, r7.yzwy
  max r4.x, r4.x, l(0.000100)  // r4.x <- streams.NdotL_id47

#line 454
  mul r7.yzw, r4.xxxx, r8.xxyz  // r7.y <- streams.lightColorNdotL_id43.x; r7.z <- streams.lightColorNdotL_id43.y; r7.w <- streams.lightColorNdotL_id43.z

#line 474
  mul r7.yzw, r0.yyzw, r7.yyzw

#line 609
  mad r5.xyz, r7.yzwy, l(0.318310, 0.318310, 0.318310, 0.000000), r5.xyzx

#line 599
  iadd r4.w, r4.w, l(1)

#line 611
endloop   // r1.w <- streams.lightIndex_id90

#line 440
mul r0.xyz, r2.xyzx, cb1[26].xyzx  // r0.x <- <ComputeEnvironmentLightContribution_id166 return value>.x; r0.y <- <ComputeEnvironmentLightContribution_id166 return value>.y; r0.z <- <ComputeEnvironmentLightContribution_id166 return value>.z

#line 630
mad o0.xyz, r5.xyzx, l(3.141593, 3.141593, 3.141593, 0.000000), r0.xyzx

#line 789
mov o0.w, r2.w
ret 
// Approximately 115 instruction slots used
***************************
*************************/
static const float PI_id180 = 3.14159265358979323846;
const static bool TIsEnergyConservative_id181 = false;
struct PS_STREAMS 
{
    float4 ScreenPosition_id83;
    float3 normalWS_id18;
    float4 PositionWS_id21;
    float2 TexCoord_id104;
    float4 ShadingPosition_id0;
    bool IsFrontFace_id1;
    float3 meshNormalWS_id16;
    float3 viewWS_id66;
    float3 shadingColor_id71;
    float matBlend_id39;
    float3 matNormal_id49;
    float4 matColorBase_id50;
    float4 matDiffuse_id51;
    float3 matDiffuseVisible_id67;
    float3 matSpecular_id53;
    float3 matSpecularVisible_id69;
    float matSpecularIntensity_id54;
    float matGlossiness_id52;
    float alphaRoughness_id68;
    float matAmbientOcclusion_id55;
    float matAmbientOcclusionDirectLightingFactor_id56;
    float matCavity_id57;
    float matCavityDiffuse_id58;
    float matCavitySpecular_id59;
    float4 matEmissive_id60;
    float matEmissiveIntensity_id61;
    float matScatteringStrength_id62;
    float2 matDiffuseSpecularAlphaBlend_id63;
    float3 matAlphaBlendColor_id64;
    float matAlphaDiscard_id65;
    float shadingColorAlpha_id72;
    float3 lightPositionWS_id40;
    float3 lightDirectionWS_id41;
    float3 lightColor_id42;
    float3 lightColorNdotL_id43;
    float3 lightSpecularColorNdotL_id44;
    float3 envLightDiffuseColor_id45;
    float3 envLightSpecularColor_id46;
    float lightDirectAmbientOcclusion_id48;
    float NdotL_id47;
    float NdotV_id70;
    uint2 lightData_id89;
    int lightIndex_id90;
    float thicknessWS_id82;
    float3 shadowColor_id81;
    float3 H_id73;
    float NdotH_id74;
    float LdotH_id75;
    float VdotH_id76;
    float4 ColorTarget_id2;
};
struct PS_OUTPUT 
{
    float4 ColorTarget_id2 : SV_Target0;
};
struct PS_INPUT 
{
    float4 PositionWS_id21 : POSITION_WS;
    float4 ShadingPosition_id0 : SV_Position;
    float3 normalWS_id18 : NORMALWS;
    float4 ScreenPosition_id83 : SCREENPOSITION_ID83_SEM;
    float2 TexCoord_id104 : TEXCOORD0;
    bool IsFrontFace_id1 : SV_IsFrontFace;
};
struct VS_STREAMS 
{
    float4 Position_id20;
    float3 meshNormal_id15;
    float2 TexCoord_id104;
    float4 PositionH_id23;
    float DepthVS_id22;
    float3 meshNormalWS_id16;
    float4 PositionWS_id21;
    float4 ShadingPosition_id0;
    float3 normalWS_id18;
    float4 ScreenPosition_id83;
};
struct VS_OUTPUT 
{
    float4 PositionWS_id21 : POSITION_WS;
    float4 ShadingPosition_id0 : SV_Position;
    float3 normalWS_id18 : NORMALWS;
    float4 ScreenPosition_id83 : SCREENPOSITION_ID83_SEM;
    float2 TexCoord_id104 : TEXCOORD0;
};
struct VS_INPUT 
{
    float4 Position_id20 : POSITION;
    float3 meshNormal_id15 : NORMAL;
    float2 TexCoord_id104 : TEXCOORD0;
};
struct PointLightData 
{
    float3 PositionWS;
    float InvSquareRadius;
    float3 Color;
};
struct PointLightDataInternal 
{
    float3 PositionWS;
    float InvSquareRadius;
    float3 Color;
};
struct SpotLightDataInternal 
{
    float3 PositionWS;
    float3 DirectionWS;
    float3 AngleOffsetAndInvSquareRadius;
    float3 Color;
};
struct SpotLightData 
{
    float3 PositionWS;
    float3 DirectionWS;
    float3 AngleOffsetAndInvSquareRadius;
    float3 Color;
};
cbuffer PerDraw 
{
    float4x4 World_id31;
    float4x4 WorldInverse_id32;
    float4x4 WorldInverseTranspose_id33;
    float4x4 WorldView_id34;
    float4x4 WorldViewInverse_id35;
    float4x4 WorldViewProjection_id36;
    float3 WorldScale_id37;
    float4 EyeMS_id38;
};
cbuffer PerMaterial 
{
    float4 constantColor_id99;
    float2 scale_id105;
    float2 offset_id106;
    float constantFloat_id168;
    float2 scale_id171;
    float2 offset_id172;
};
cbuffer PerView 
{
    float4x4 View_id24;
    float4x4 ViewInverse_id25;
    float4x4 Projection_id26;
    float4x4 ProjectionInverse_id27;
    float4x4 ViewProjection_id28;
    float2 ProjScreenRay_id29;
    float4 Eye_id30;
    float NearClipPlane_id84 = 1.0f;
    float FarClipPlane_id85 = 100.0f;
    float2 ZProjection_id86;
    float2 ViewSize_id87;
    float AspectRatio_id88;
    float4 _padding_PerView_Default;
    float ClusterDepthScale_id93;
    float ClusterDepthBias_id94;
    float2 ClusterStride_id95;
    float3 AmbientLight_id98;
    float4 _padding_PerView_Lighting;
};
cbuffer Globals 
{
    float2 Texture0TexelSize_id111;
    float2 Texture1TexelSize_id113;
    float2 Texture2TexelSize_id115;
    float2 Texture3TexelSize_id117;
    float2 Texture4TexelSize_id119;
    float2 Texture5TexelSize_id121;
    float2 Texture6TexelSize_id123;
    float2 Texture7TexelSize_id125;
    float2 Texture8TexelSize_id127;
    float2 Texture9TexelSize_id129;
};
Texture2D Texture0_id110;
Texture2D Texture1_id112;
Texture2D Texture2_id114;
Texture2D Texture3_id116;
Texture2D Texture4_id118;
Texture2D Texture5_id120;
Texture2D Texture6_id122;
Texture2D Texture7_id124;
Texture2D Texture8_id126;
Texture2D Texture9_id128;
TextureCube TextureCube0_id130;
TextureCube TextureCube1_id131;
TextureCube TextureCube2_id132;
TextureCube TextureCube3_id133;
Texture3D Texture3D0_id134;
Texture3D Texture3D1_id135;
Texture3D Texture3D2_id136;
Texture3D Texture3D3_id137;
SamplerState Sampler_id138;
SamplerState PointSampler_id139 
{
    Filter = MIN_MAG_MIP_POINT;
};
SamplerState LinearSampler_id140 
{
    Filter = MIN_MAG_MIP_LINEAR;
};
SamplerState LinearBorderSampler_id141 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Border;
    AddressV = Border;
};
SamplerComparisonState LinearClampCompareLessEqualSampler_id142 
{
    Filter = COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
    ComparisonFunc = LessEqual;
};
SamplerState AnisotropicSampler_id143 
{
    Filter = ANISOTROPIC;
};
SamplerState AnisotropicRepeatSampler_id144 
{
    Filter = ANISOTROPIC;
    AddressU = Wrap;
    AddressV = Wrap;
    MaxAnisotropy = 16;
};
SamplerState PointRepeatSampler_id145 
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState LinearRepeatSampler_id146 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState RepeatSampler_id147 
{
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState Sampler0_id148;
SamplerState Sampler1_id149;
SamplerState Sampler2_id150;
SamplerState Sampler3_id151;
SamplerState Sampler4_id152;
SamplerState Sampler5_id153;
SamplerState Sampler6_id154;
SamplerState Sampler7_id155;
SamplerState Sampler8_id156;
SamplerState Sampler9_id157;
Texture2D Texture_id102;
SamplerState Sampler_id103;
Texture2D Texture_id169;
Texture3D<uint2> LightClusters_id91;
Buffer<uint> LightIndices_id92;
Buffer<float4> PointLights_id96;
Buffer<float4> SpotLights_id97;
float SmoothDistanceAttenuation_id59(float squaredDistance, float lightInvSquareRadius)
{
    float factor = squaredDistance * lightInvSquareRadius;
    float smoothFactor = saturate(1.0f - factor * factor);
    return smoothFactor * smoothFactor;
}
float SmoothDistanceAttenuation_id48(float squaredDistance, float lightInvSquareRadius)
{
    float factor = squaredDistance * lightInvSquareRadius;
    float smoothFactor = saturate(1.0f - factor * factor);
    return smoothFactor * smoothFactor;
}
float GetAngularAttenuation_id61(float3 lightVector, float3 lightDirection, float lightAngleScale, float lightAngleOffset)
{
    float cd = dot(lightDirection, lightVector);
    float attenuation = saturate(cd * lightAngleScale + lightAngleOffset);
    attenuation *= attenuation;
    return attenuation;
}
float GetDistanceAttenuation_id60(float lightVectorLength, float lightInvSquareRadius)
{
    float d2 = lightVectorLength * lightVectorLength;
    float attenuation = 1.0 / (max(d2, 0.01 * 0.01));
    attenuation *= SmoothDistanceAttenuation_id59(d2, lightInvSquareRadius);
    return attenuation;
}
float GetDistanceAttenuation_id50(float lightVectorLength, float lightInvSquareRadius)
{
    float d2 = lightVectorLength * lightVectorLength;
    float attenuation = 1.0 / (max(d2, 0.01 * 0.01));
    attenuation *= SmoothDistanceAttenuation_id48(d2, lightInvSquareRadius);
    return attenuation;
}
float ComputeAttenuation_id62(float3 PositionWS, float3 AngleOffsetAndInvSquareRadius, float3 DirectionWS, float3 position, inout float3 lightVectorNorm)
{
    float3 lightVector = PositionWS - position;
    float lightVectorLength = length(lightVector);
    lightVectorNorm = lightVector / lightVectorLength;
    float3 lightAngleOffsetAndInvSquareRadius = AngleOffsetAndInvSquareRadius;
    float2 lightAngleAndOffset = lightAngleOffsetAndInvSquareRadius.xy;
    float lightInvSquareRadius = lightAngleOffsetAndInvSquareRadius.z;
    float3 lightDirection = -DirectionWS;
    float attenuation = GetDistanceAttenuation_id60(lightVectorLength, lightInvSquareRadius);
    attenuation *= GetAngularAttenuation_id61(lightVectorNorm, lightDirection, lightAngleAndOffset.x, lightAngleAndOffset.y);
    return attenuation;
}
float ComputeAttenuation_id49(PointLightDataInternal light, float3 position, inout float3 lightVectorNorm)
{
    float3 lightVector = light.PositionWS - position;
    float lightVectorLength = length(lightVector);
    lightVectorNorm = lightVector / lightVectorLength;
    float lightInvSquareRadius = light.InvSquareRadius;
    return GetDistanceAttenuation_id50(lightVectorLength, lightInvSquareRadius);
}
float4 Compute_id148(inout PS_STREAMS streams)
{
    return Texture_id169.Sample(Sampler_id103, streams.TexCoord_id104 * scale_id171 + offset_id172).r;
}
float4 Compute_id146()
{
    return float4(constantFloat_id168, constantFloat_id168, constantFloat_id168, constantFloat_id168);
}
void ProcessLight_id63(inout PS_STREAMS streams, SpotLightDataInternal light)
{
    float3 lightVectorNorm;
    float attenuation = ComputeAttenuation_id62(light.PositionWS, light.AngleOffsetAndInvSquareRadius, light.DirectionWS, streams.PositionWS_id21.xyz, lightVectorNorm);
    streams.lightColor_id42 = light.Color * attenuation;
    streams.lightDirectionWS_id41 = lightVectorNorm;
}
void ProcessLight_id52(inout PS_STREAMS streams, PointLightDataInternal light)
{
    float3 lightVectorNorm;
    float attenuation = ComputeAttenuation_id49(light, streams.PositionWS_id21.xyz, lightVectorNorm);
    streams.lightPositionWS_id40 = light.PositionWS;
    streams.lightColor_id42 = light.Color * attenuation;
    streams.lightDirectionWS_id41 = lightVectorNorm;
}
float4 Compute_id151(inout PS_STREAMS streams)
{
    float4 tex1 = Compute_id146();
    float4 tex2 = Compute_id148(streams);
    float4 mix1 = tex1 * tex2;
    return mix1;
}
float4 Compute_id144(inout PS_STREAMS streams)
{
    return Texture_id102.Sample(Sampler_id103, streams.TexCoord_id104 * scale_id105 + offset_id106).rgba;
}
void PrepareEnvironmentLight_id67(inout PS_STREAMS streams)
{
    streams.envLightDiffuseColor_id45 = 0;
    streams.envLightSpecularColor_id46 = 0;
}
float3 ComputeSpecularTextureProjection_id58(float3 positionWS, float3 reflectionWS, int lightIndex)
{
    return 1.0f;
}
float3 ComputeTextureProjection_id57(float3 positionWS, int lightIndex)
{
    return 1.0f;
}
float3 ComputeShadow_id56(inout PS_STREAMS streams, float3 position, int lightIndex)
{
    streams.thicknessWS_id82 = 0.0;
    return 1.0f;
}
void PrepareDirectLightCore_id55(inout PS_STREAMS streams, int lightIndexIgnored)
{
    int realLightIndex = LightIndices_id92.Load(streams.lightIndex_id90);
    streams.lightIndex_id90++;
    SpotLightDataInternal spotLight;
    float4 spotLight1 = SpotLights_id97.Load(realLightIndex * 4);
    float4 spotLight2 = SpotLights_id97.Load(realLightIndex * 4 + 1);
    float4 spotLight3 = SpotLights_id97.Load(realLightIndex * 4 + 2);
    float4 spotLight4 = SpotLights_id97.Load(realLightIndex * 4 + 3);
    spotLight.PositionWS = spotLight1.xyz;
    spotLight.DirectionWS = spotLight2.xyz;
    spotLight.AngleOffsetAndInvSquareRadius = spotLight3.xyz;
    spotLight.Color = spotLight4.xyz;
    ProcessLight_id63(streams, spotLight);
}
float3 ComputeSpecularTextureProjection_id47(float3 positionWS, float3 reflectionWS, int lightIndex)
{
    return 1.0f;
}
float3 ComputeTextureProjection_id46(float3 positionWS, int lightIndex)
{
    return 1.0f;
}
float3 ComputeShadow_id45(inout PS_STREAMS streams, float3 position, int lightIndex)
{
    streams.thicknessWS_id82 = 0.0;
    return 1.0f;
}
void PrepareDirectLightCore_id44(inout PS_STREAMS streams, int lightIndexIgnored)
{
    int realLightIndex = LightIndices_id92.Load(streams.lightIndex_id90);
    streams.lightIndex_id90++;
    PointLightDataInternal pointLight;
    float4 pointLight1 = PointLights_id96.Load(realLightIndex * 2);
    float4 pointLight2 = PointLights_id96.Load(realLightIndex * 2 + 1);
    pointLight.PositionWS = pointLight1.xyz;
    pointLight.InvSquareRadius = pointLight1.w;
    pointLight.Color = pointLight2.xyz;
    ProcessLight_id52(streams, pointLight);
}
void PrepareLightData_id51(inout PS_STREAMS streams)
{
    float projectedDepth = streams.ShadingPosition_id0.z;
    float depth = ZProjection_id86.y / (projectedDepth - ZProjection_id86.x);
    float2 texCoord = float2(streams.ScreenPosition_id83.x + 1, 1 - streams.ScreenPosition_id83.y) * 0.5;
    int slice = int(max(log2(depth * ClusterDepthScale_id93 + ClusterDepthBias_id94), 0));
    streams.lightData_id89 = LightClusters_id91.Load(int4(texCoord * ClusterStride_id95, slice, 0));
    streams.lightIndex_id90 = streams.lightData_id89.x;
}
void Compute_id161(inout PS_STREAMS streams)
{
    streams.matBlend_id39 = Compute_id151(streams).r;
}
void Compute_id156(inout PS_STREAMS streams)
{
    float4 colorBase = Compute_id144(streams);
    streams.matDiffuse_id51 = colorBase;
    streams.matColorBase_id50 = colorBase;
}
void ResetStream_id96()
{
}
void AfterLightingAndShading_id173()
{
}
void PrepareEnvironmentLight_id131(inout PS_STREAMS streams)
{
    streams.envLightDiffuseColor_id45 = 0;
    streams.envLightSpecularColor_id46 = 0;
}
float3 ComputeEnvironmentLightContribution_id166(inout PS_STREAMS streams)
{
    float3 diffuseColor = streams.matDiffuseVisible_id67;
    return diffuseColor * streams.envLightDiffuseColor_id45;
}
void PrepareEnvironmentLight_id128(inout PS_STREAMS streams)
{
    PrepareEnvironmentLight_id67(streams);
    float3 lightColor = AmbientLight_id98 * streams.matAmbientOcclusion_id55;
    streams.envLightDiffuseColor_id45 = lightColor;
    streams.envLightSpecularColor_id46 = lightColor;
}
void PrepareDirectLight_id119(inout PS_STREAMS streams, int lightIndex)
{
    PrepareDirectLightCore_id55(streams, lightIndex);
    streams.NdotL_id47 = max(dot(streams.normalWS_id18, streams.lightDirectionWS_id41), 0.0001f);
    streams.shadowColor_id81 = ComputeShadow_id56(streams, streams.PositionWS_id21.xyz, lightIndex);
    streams.lightColorNdotL_id43 = streams.lightColor_id42 * streams.shadowColor_id81 * streams.NdotL_id47 * streams.lightDirectAmbientOcclusion_id48;
    streams.lightSpecularColorNdotL_id44 = streams.lightColorNdotL_id43;
    streams.lightColorNdotL_id43 *= ComputeTextureProjection_id57(streams.PositionWS_id21.xyz, lightIndex);
    float3 reflectionVectorWS = reflect(-streams.viewWS_id66, streams.normalWS_id18);
    streams.lightSpecularColorNdotL_id44 *= ComputeSpecularTextureProjection_id58(streams.PositionWS_id21.xyz, reflectionVectorWS, lightIndex);
}
int GetLightCount_id123(inout PS_STREAMS streams)
{
    return streams.lightData_id89.y >> 16;
}
int GetMaxLightCount_id122(inout PS_STREAMS streams)
{
    return streams.lightData_id89.y >> 16;
}
void PrepareDirectLights_id117()
{
}
float3 ComputeDirectLightContribution_id165(inout PS_STREAMS streams)
{
    float3 diffuseColor = streams.matDiffuseVisible_id67;
    return diffuseColor / PI_id180 * streams.lightColorNdotL_id43 * streams.matDiffuseSpecularAlphaBlend_id63.x;
}
void PrepareMaterialPerDirectLight_id30(inout PS_STREAMS streams)
{
    streams.H_id73 = normalize(streams.viewWS_id66 + streams.lightDirectionWS_id41);
    streams.NdotH_id74 = saturate(dot(streams.normalWS_id18, streams.H_id73));
    streams.LdotH_id75 = saturate(dot(streams.lightDirectionWS_id41, streams.H_id73));
    streams.VdotH_id76 = streams.LdotH_id75;
}
void PrepareDirectLight_id106(inout PS_STREAMS streams, int lightIndex)
{
    PrepareDirectLightCore_id44(streams, lightIndex);
    streams.NdotL_id47 = max(dot(streams.normalWS_id18, streams.lightDirectionWS_id41), 0.0001f);
    streams.shadowColor_id81 = ComputeShadow_id45(streams, streams.PositionWS_id21.xyz, lightIndex);
    streams.lightColorNdotL_id43 = streams.lightColor_id42 * streams.shadowColor_id81 * streams.NdotL_id47 * streams.lightDirectAmbientOcclusion_id48;
    streams.lightSpecularColorNdotL_id44 = streams.lightColorNdotL_id43;
    streams.lightColorNdotL_id43 *= ComputeTextureProjection_id46(streams.PositionWS_id21.xyz, lightIndex);
    float3 reflectionVectorWS = reflect(-streams.viewWS_id66, streams.normalWS_id18);
    streams.lightSpecularColorNdotL_id44 *= ComputeSpecularTextureProjection_id47(streams.PositionWS_id21.xyz, reflectionVectorWS, lightIndex);
}
int GetLightCount_id111(inout PS_STREAMS streams)
{
    return streams.lightData_id89.y & 0xFFFF;
}
int GetMaxLightCount_id110(inout PS_STREAMS streams)
{
    return streams.lightData_id89.y & 0xFFFF;
}
void PrepareDirectLights_id109(inout PS_STREAMS streams)
{
    PrepareLightData_id51(streams);
}
void PrepareForLightingAndShading_id170()
{
}
void PrepareMaterialForLightingAndShading_id95(inout PS_STREAMS streams)
{
    streams.lightDirectAmbientOcclusion_id48 = lerp(1.0, streams.matAmbientOcclusion_id55, streams.matAmbientOcclusionDirectLightingFactor_id56);
    streams.matDiffuseVisible_id67 = streams.matDiffuse_id51.rgb * lerp(1.0f, streams.matCavity_id57, streams.matCavityDiffuse_id58) * streams.matDiffuseSpecularAlphaBlend_id63.r * streams.matAlphaBlendColor_id64;
    streams.matSpecularVisible_id69 = streams.matSpecular_id53.rgb * streams.matSpecularIntensity_id54 * lerp(1.0f, streams.matCavity_id57, streams.matCavitySpecular_id59) * streams.matDiffuseSpecularAlphaBlend_id63.g * streams.matAlphaBlendColor_id64;
    streams.NdotV_id70 = max(dot(streams.normalWS_id18, streams.viewWS_id66), 0.0001f);
    float roughness = 1.0f - streams.matGlossiness_id52;
    streams.alphaRoughness_id68 = max(roughness * roughness, 0.001);
}
void ResetLightStream_id94(inout PS_STREAMS streams)
{
    streams.lightPositionWS_id40 = 0;
    streams.lightDirectionWS_id41 = 0;
    streams.lightColor_id42 = 0;
    streams.lightColorNdotL_id43 = 0;
    streams.lightSpecularColorNdotL_id44 = 0;
    streams.envLightDiffuseColor_id45 = 0;
    streams.envLightSpecularColor_id46 = 0;
    streams.lightDirectAmbientOcclusion_id48 = 1.0f;
    streams.NdotL_id47 = 0;
}
void UpdateNormalFromTangentSpace_id23(float3 normalInTangentSpace)
{
}
void Compute_id139(inout PS_STREAMS streams, PS_STREAMS fromStream)
{
    streams.matColorBase_id50 = lerp(fromStream.matColorBase_id50, streams.matColorBase_id50, streams.matBlend_id39);
}
void Compute_id134(inout PS_STREAMS streams, PS_STREAMS fromStream)
{
    streams.matDiffuse_id51 = lerp(fromStream.matDiffuse_id51, streams.matDiffuse_id51, streams.matBlend_id39);
}
void Compute_id164(inout PS_STREAMS streams)
{

    {
        Compute_id156(streams);
    }

    {
        Compute_id161(streams);
    }
}
float4 Compute_id132()
{
    return constantColor_id99;
}
void ResetStream_id97(inout PS_STREAMS streams)
{
    ResetStream_id96();
    streams.matBlend_id39 = 0.0f;
}
void Compute_id210(inout PS_STREAMS streams)
{
    UpdateNormalFromTangentSpace_id23(streams.matNormal_id49);
    if (!streams.IsFrontFace_id1)
        streams.normalWS_id18 = -streams.normalWS_id18;
    ResetLightStream_id94(streams);
    PrepareMaterialForLightingAndShading_id95(streams);

    {
        PrepareForLightingAndShading_id170();
    }
    float3 directLightingContribution = 0;

    {
        PrepareDirectLights_id109(streams);
        const int maxLightCount = GetMaxLightCount_id110(streams);
        int count = GetLightCount_id111(streams);

        for (int i = 0; i < maxLightCount; i++)
        {
            if (i >= count)
            {
                break;
            }
            PrepareDirectLight_id106(streams, i);
            PrepareMaterialPerDirectLight_id30(streams);

            {
                directLightingContribution += ComputeDirectLightContribution_id165(streams);
            }
        }
    }

    {
        PrepareDirectLights_id117();
        const int maxLightCount = GetMaxLightCount_id122(streams);
        int count = GetLightCount_id123(streams);

        for (int i = 0; i < maxLightCount; i++)
        {
            if (i >= count)
            {
                break;
            }
            PrepareDirectLight_id119(streams, i);
            PrepareMaterialPerDirectLight_id30(streams);

            {
                directLightingContribution += ComputeDirectLightContribution_id165(streams);
            }
        }
    }
    float3 environmentLightingContribution = 0;

    {
        PrepareEnvironmentLight_id128(streams);

        {
            environmentLightingContribution += ComputeEnvironmentLightContribution_id166(streams);
        }
    }

    {
        PrepareEnvironmentLight_id131(streams);

        {
            environmentLightingContribution += ComputeEnvironmentLightContribution_id166(streams);
        }
    }
    streams.shadingColor_id71 += directLightingContribution * PI_id180 + environmentLightingContribution;
    streams.shadingColorAlpha_id72 = streams.matDiffuse_id51.a;

    {
        AfterLightingAndShading_id173();
    }
}
void Compute_id194(inout PS_STREAMS streams)
{
    PS_STREAMS backup = streams;
    Compute_id164(streams);

    {
        Compute_id134(streams, backup);
    }

    {
        Compute_id139(streams, backup);
    }
}
void Compute_id191(inout PS_STREAMS streams)
{
    float4 colorBase = Compute_id132();
    streams.matDiffuse_id51 = colorBase;
    streams.matColorBase_id50 = colorBase;
}
void ResetStream_id98(inout PS_STREAMS streams)
{
    ResetStream_id97(streams);
    streams.matNormal_id49 = float3(0, 0, 1);
    streams.matColorBase_id50 = 0.0f;
    streams.matDiffuse_id51 = 0.0f;
    streams.matDiffuseVisible_id67 = 0.0f;
    streams.matSpecular_id53 = 0.0f;
    streams.matSpecularVisible_id69 = 0.0f;
    streams.matSpecularIntensity_id54 = 1.0f;
    streams.matGlossiness_id52 = 0.0f;
    streams.alphaRoughness_id68 = 1.0f;
    streams.matAmbientOcclusion_id55 = 1.0f;
    streams.matAmbientOcclusionDirectLightingFactor_id56 = 0.0f;
    streams.matCavity_id57 = 1.0f;
    streams.matCavityDiffuse_id58 = 0.0f;
    streams.matCavitySpecular_id59 = 0.0f;
    streams.matEmissive_id60 = 0.0f;
    streams.matEmissiveIntensity_id61 = 0.0f;
    streams.matScatteringStrength_id62 = 1.0f;
    streams.matDiffuseSpecularAlphaBlend_id63 = 1.0f;
    streams.matAlphaBlendColor_id64 = 1.0f;
    streams.matAlphaDiscard_id65 = 0.1f;
}
float4 ComputeShadingPosition_id11(float4 world)
{
    return mul(world, ViewProjection_id28);
}
void PostTransformPosition_id6()
{
}
void PreTransformPosition_id4()
{
}
void Compute_id41(inout PS_STREAMS streams)
{

    {
        Compute_id191(streams);
    }

    {
        Compute_id194(streams);
    }

    {
        Compute_id210(streams);
    }
}
void ResetStream_id40(inout PS_STREAMS streams)
{
    ResetStream_id98(streams);
    streams.shadingColorAlpha_id72 = 1.0f;
}
void PostTransformPosition_id12(inout VS_STREAMS streams)
{
    PostTransformPosition_id6();
    streams.ShadingPosition_id0 = ComputeShadingPosition_id11(streams.PositionWS_id21);
    streams.PositionH_id23 = streams.ShadingPosition_id0;
    streams.DepthVS_id22 = streams.ShadingPosition_id0.w;
}
void TransformPosition_id5()
{
}
void PreTransformPosition_id10(inout VS_STREAMS streams)
{
    PreTransformPosition_id4();
    streams.PositionWS_id21 = mul(streams.Position_id20, World_id31);
}
float4 Shading_id31(inout PS_STREAMS streams)
{
    streams.viewWS_id66 = normalize(Eye_id30.xyz - streams.PositionWS_id21.xyz);
    streams.shadingColor_id71 = 0;
    ResetStream_id40(streams);
    Compute_id41(streams);
    return float4(streams.shadingColor_id71, streams.shadingColorAlpha_id72);
}
void PSMain_id1()
{
}
void BaseTransformVS_id8(inout VS_STREAMS streams)
{
    PreTransformPosition_id10(streams);
    TransformPosition_id5();
    PostTransformPosition_id12(streams);
}
void VSMain_id0()
{
}
void PSMain_id3(inout PS_STREAMS streams)
{
    PSMain_id1();
    streams.ColorTarget_id2 = Shading_id31(streams);
}
void GenerateNormal_PS_id22(inout PS_STREAMS streams)
{
    if (dot(streams.normalWS_id18, streams.normalWS_id18) > 0)
        streams.normalWS_id18 = normalize(streams.normalWS_id18);
    streams.meshNormalWS_id16 = streams.normalWS_id18;
}
void GenerateNormal_VS_id21(inout VS_STREAMS streams)
{
    streams.meshNormalWS_id16 = mul(streams.meshNormal_id15, (float3x3)WorldInverseTranspose_id33);
    streams.normalWS_id18 = streams.meshNormalWS_id16;
}
void VSMain_id9(inout VS_STREAMS streams)
{
    VSMain_id0();
    BaseTransformVS_id8(streams);
}
void PSMain_id20(inout PS_STREAMS streams)
{
    GenerateNormal_PS_id22(streams);
    PSMain_id3(streams);
}
void VSMain_id19(inout VS_STREAMS streams)
{
    VSMain_id9(streams);
    GenerateNormal_VS_id21(streams);
}
PS_OUTPUT PSMain(PS_INPUT __input__)
{
    PS_STREAMS streams = (PS_STREAMS)0;
    streams.PositionWS_id21 = __input__.PositionWS_id21;
    streams.ShadingPosition_id0 = __input__.ShadingPosition_id0;
    streams.normalWS_id18 = __input__.normalWS_id18;
    streams.ScreenPosition_id83 = __input__.ScreenPosition_id83;
    streams.TexCoord_id104 = __input__.TexCoord_id104;
    streams.IsFrontFace_id1 = __input__.IsFrontFace_id1;
    streams.ScreenPosition_id83 /= streams.ScreenPosition_id83.w;
    PSMain_id20(streams);
    PS_OUTPUT __output__ = (PS_OUTPUT)0;
    __output__.ColorTarget_id2 = streams.ColorTarget_id2;
    return __output__;
}
VS_OUTPUT VSMain(VS_INPUT __input__)
{
    VS_STREAMS streams = (VS_STREAMS)0;
    streams.Position_id20 = __input__.Position_id20;
    streams.meshNormal_id15 = __input__.meshNormal_id15;
    streams.TexCoord_id104 = __input__.TexCoord_id104;
    VSMain_id19(streams);
    streams.ScreenPosition_id83 = streams.ShadingPosition_id0;
    VS_OUTPUT __output__ = (VS_OUTPUT)0;
    __output__.PositionWS_id21 = streams.PositionWS_id21;
    __output__.ShadingPosition_id0 = streams.ShadingPosition_id0;
    __output__.normalWS_id18 = streams.normalWS_id18;
    __output__.ScreenPosition_id83 = streams.ScreenPosition_id83;
    __output__.TexCoord_id104 = streams.TexCoord_id104;
    return __output__;
}
