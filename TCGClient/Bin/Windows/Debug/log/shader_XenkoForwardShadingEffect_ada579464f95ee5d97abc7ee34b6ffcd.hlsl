/**************************
***** Compiler Parameters *****
***************************
@P EffectName: XenkoForwardShadingEffect
@P   - Material.PixelStageSurfaceShaders: mixin MaterialSurfaceArray [{layers = [mixin MaterialSurfaceDiffuse [{diffuseMap = ComputeColorConstantColorLink<Material.DiffuseValue>}], mixin MaterialSurfaceGlossinessMap<false> [{glossinessMap = ComputeColorConstantFloatLink<Material.GlossinessValue>}], mixin MaterialSurfaceMetalness [{metalnessMap = ComputeColorConstantFloatLink<Material.MetalnessValue>}], mixin MaterialSurfaceLightingAndShading [{surfaces = [MaterialSurfaceShadingDiffuseLambert<false>, mixin MaterialSurfaceShadingSpecularMicrofacet [{environmentFunction = MaterialSpecularMicrofacetEnvironmentGGXLUT}, {fresnelFunction = MaterialSpecularMicrofacetFresnelSchlick}, {geometricShadowingFunction = MaterialSpecularMicrofacetVisibilitySmithSchlickGGX}, {normalDistributionFunction = MaterialSpecularMicrofacetNormalDistributionGGX}]]}]]}]
@P Material.PixelStageStreamInitializer: mixin MaterialStream, MaterialPixelShadingStream
@P Lighting.DirectLightGroups: LightClusteredPointGroup, LightClusteredSpotGroup
@P Lighting.EnvironmentLights: LightSimpleAmbient, EnvironmentLight
@P XenkoEffectBase.RenderTargetExtensions: mixin
***************************
****  ConstantBuffers  ****
***************************
cbuffer PerDraw [Size: 416]
@C    World_id31 => Transformation.World
@C    WorldInverse_id32 => Transformation.WorldInverse
@C    WorldInverseTranspose_id33 => Transformation.WorldInverseTranspose
@C    WorldView_id34 => Transformation.WorldView
@C    WorldViewInverse_id35 => Transformation.WorldViewInverse
@C    WorldViewProjection_id36 => Transformation.WorldViewProjection
@C    WorldScale_id37 => Transformation.WorldScale
@C    EyeMS_id38 => Transformation.EyeMS
cbuffer PerMaterial [Size: 32]
@C    constantColor_id99 => Material.DiffuseValue
@C    constantFloat_id101 => Material.GlossinessValue
@C    constantFloat_id104 => Material.MetalnessValue
cbuffer PerView [Size: 448]
@C    View_id24 => Transformation.View
@C    ViewInverse_id25 => Transformation.ViewInverse
@C    Projection_id26 => Transformation.Projection
@C    ProjectionInverse_id27 => Transformation.ProjectionInverse
@C    ViewProjection_id28 => Transformation.ViewProjection
@C    ProjScreenRay_id29 => Transformation.ProjScreenRay
@C    Eye_id30 => Transformation.Eye
@C    NearClipPlane_id84 => Camera.NearClipPlane
@C    FarClipPlane_id85 => Camera.FarClipPlane
@C    ZProjection_id86 => Camera.ZProjection
@C    ViewSize_id87 => Camera.ViewSize
@C    AspectRatio_id88 => Camera.AspectRatio
@C    _padding_PerView_Default => _padding_PerView_Default
@C    ClusterDepthScale_id93 => LightClustered.ClusterDepthScale
@C    ClusterDepthBias_id94 => LightClustered.ClusterDepthBias
@C    ClusterStride_id95 => LightClustered.ClusterStride
@C    AmbientLight_id98 => LightSimpleAmbient.AmbientLight.environmentLights[0]
@C    _padding_PerView_Lighting => _padding_PerView_Lighting
***************************
******  Resources    ******
***************************
@R    PerMaterial => PerMaterial [Stage: None, Slot: (-1--1)]
@R    EnvironmentLightingDFG_LUT_id169 => MaterialSpecularMicrofacetEnvironmentGGXLUT.EnvironmentLightingDFG_LUT [Stage: None, Slot: (-1--1)]
@R    EnvironmentLightingDFG_LUT_id169 => MaterialSpecularMicrofacetEnvironmentGGXLUT.EnvironmentLightingDFG_LUT [Stage: None, Slot: (-1--1)]
@R    LightClusters_id91 => LightClustered.LightClusters [Stage: None, Slot: (-1--1)]
@R    LightClusters_id91 => LightClustered.LightClusters [Stage: None, Slot: (-1--1)]
@R    LightIndices_id92 => LightClustered.LightIndices [Stage: None, Slot: (-1--1)]
@R    LightIndices_id92 => LightClustered.LightIndices [Stage: None, Slot: (-1--1)]
@R    PointLights_id96 => LightClusteredPointGroup.PointLights [Stage: None, Slot: (-1--1)]
@R    PointLights_id96 => LightClusteredPointGroup.PointLights [Stage: None, Slot: (-1--1)]
@R    SpotLights_id97 => LightClusteredSpotGroup.SpotLights [Stage: None, Slot: (-1--1)]
@R    SpotLights_id97 => LightClusteredSpotGroup.SpotLights [Stage: None, Slot: (-1--1)]
@R    PerDraw => PerDraw [Stage: Vertex, Slot: (0-0)]
@R    PerView => PerView [Stage: Vertex, Slot: (1-1)]
@R    LinearSampler_id141 => Texturing.LinearSampler [Stage: Pixel, Slot: (0-0)]
@R    EnvironmentLightingDFG_LUT_id169 => MaterialSpecularMicrofacetEnvironmentGGXLUT.EnvironmentLightingDFG_LUT [Stage: Pixel, Slot: (0-0)]
@R    LightClusters_id91 => LightClustered.LightClusters [Stage: Pixel, Slot: (1-1)]
@R    LightIndices_id92 => LightClustered.LightIndices [Stage: Pixel, Slot: (2-2)]
@R    PointLights_id96 => LightClusteredPointGroup.PointLights [Stage: Pixel, Slot: (3-3)]
@R    SpotLights_id97 => LightClusteredSpotGroup.SpotLights [Stage: Pixel, Slot: (4-4)]
@R    PerMaterial => PerMaterial [Stage: Pixel, Slot: (0-0)]
@R    PerView => PerView [Stage: Pixel, Slot: (1-1)]
***************************
*****     Sources     *****
***************************
@S    ShaderBase => acbe3d4d44a046eede871176bee9c754
@S    ShaderBaseStream => a3a5bf8185f2a3d89972293f806430d3
@S    ShadingBase => a56c21640e78c756e8d56651480eb9f5
@S    ComputeColor => ded06879812e042b84d284d2272e4b4a
@S    TransformationBase => be8628f6067b518dd5c7b3fe338b9320
@S    NormalStream => b59c6ee174b93be981cc113a7a70d70b
@S    TransformationWAndVP => 37eaa3c16c9e83fd77e04c47c6794803
@S    PositionStream4 => b1f2243b30eb87e6e40bc6af56b4fd18
@S    PositionHStream4 => 0c0bb9059a8e3199d9ec950f3a7d2b66
@S    Transformation => ce8c4a6980d1f949f5f9aed15679c96d
@S    NormalFromMesh => 7e93d74a2c2c59456ebca897ee2c4bdb
@S    NormalBase => 118edc0075e4c3a87c3cb4570a808039
@S    NormalUpdate => 9bf3758fe45b1105750554fed463a15e
@S    MaterialSurfacePixelStageCompositor => cb7b21ca902e1b289c9d765318058f5a
@S    PositionStream => d767885dace5d697c6532bb4f0f5b3f8
@S    MaterialPixelShadingStream => ce423644e4da2371ef9e6b451c39edf7
@S    MaterialPixelStream => 83c4d63f50d89eb133457f26bcc25822
@S    MaterialStream => f4c30b25d4f10a4a3809b97598bfca17
@S    IStreamInitializer => 37d0ab08af9e2896a54aa43c0ea2ad0f
@S    LightStream => 622a7adc4e53980a3d0e0fecffc76661
@S    DirectLightGroupArray => b6017f14fd58343b7a54fefa2ca81610
@S    DirectLightGroup => 08ae1198dfb5788ff8584d747c3b8368
@S    ShadowGroup => 05cfb339de033879838a6606f047a866
@S    ShadowStream => 38650f3182fa7d3cab3bae43f9398f4e
@S    TextureProjectionGroup => 47f8334211d8d5ae6d98f32a923759cc
@S    EnvironmentLightArray => 4129555ea2051a98f5cf5e315d791d3e
@S    EnvironmentLight => 6aceaad54f2057382904d7f6dfa58e4f
@S    IMaterialSurface => d56637e1116951bd72b1817daa1a6158
@S    LightClusteredPointGroup => c5703508c16a19f7acb9ebe6676eeb4a
@S    LightClustered => 82f6bcf6878ecb8f1cf19364969848f1
@S    ScreenPositionBase => b66b925e44d5cef758dd09d560d88680
@S    Camera => 38d8cc7176ac62fc4d9c97d70f9013ab
@S    LightPoint => 5cc6bc739ab0f656d8a48332e8ca569b
@S    LightUtil => 2587d00dc291bbace485f1420ae464ad
@S    LightClusteredSpotGroup => d9e6bd2fe470894c4d8bebba13305bf2
@S    LightSpot => ff13b9737bc99d2458fa5aa679b6b8f0
@S    SpotLightDataInternalShader => 4c9afecddd31668dbea2f90869a2d5f0
@S    LightSpotAttenuationDefault => a424c87e1fb86625b52e4ab0ab01bf46
@S    LightSimpleAmbient => 5216c2bc901f0543bc29393671a09451
@S    MaterialSurfaceArray => ca7a8b492198ae093d4f490c2ba6aaec
@S    MaterialSurfaceDiffuse => c60d7ca8058e062fcba77cc9b4c7d496
@S    IMaterialSurfacePixel => b5f583c7b871b6a4ebe5c7411883503e
@S    ComputeColorConstantColorLink => a30d6cd76f7c0875cdeffdb3ae8bae33
@S    MaterialSurfaceGlossinessMap => d7edef2f457bc2e68a2644819057820d
@S    ComputeColorConstantFloatLink => 12410f8b57e4015ff1a3fce241440b62
@S    MaterialSurfaceMetalness => bf2527d0291c998be31cb1aa6e8df94c
@S    MaterialSurfaceLightingAndShading => 68ac8c9f25624fdc15ea262fbc868030
@S    Math => 49d7f7706890095f248caeaf232f4db4
@S    IMaterialSurfaceShading => 43b5938a14c30cfc19b2ddcb76824cbe
@S    MaterialSurfaceShadingDiffuseLambert => ebe9647a0e11903ef16a793ffdbdd34c
@S    MaterialSurfaceShadingSpecularMicrofacet => d73b976ae539136e12c8ad3b9c23fde4
@S    BRDFMicrofacet => 7e71ebf42695365a986502c8c568bd4c
@S    IMaterialSpecularMicrofacetFresnelFunction => 202cfaa696414bc80515f7cc9128dba0
@S    IMaterialSpecularMicrofacetVisibilityFunction => 3426c6d9f0a56bcb6863f3537df6b8dd
@S    IMaterialSpecularMicrofacetNormalDistributionFunction => b91eb150f041a287afbd1a1317f3218b
@S    IMaterialSpecularMicrofacetEnvironmentFunction => 6fe5dc0428d341b5128f0268a3b15d6e
@S    MaterialSpecularMicrofacetEnvironmentGGXLUT => 1d429af3e4e1cf62dbc90503fc7cf2bd
@S    Texturing => e6daef0dd90a55f9549c6f5d291e61a5
@S    MaterialSpecularMicrofacetFresnelSchlick => 7c4f716bc4013816bc7dfd0875fe37c0
@S    MaterialSpecularMicrofacetVisibilitySmithSchlickGGX => 786a567442fa4cebef54fd786bf5f585
@S    MaterialSpecularMicrofacetNormalDistributionGGX => db0c804e82483452e11bd0226bd029ff
***************************
*****     Stages      *****
***************************
@G    Vertex => a9643769d8808feabf4075d3c00de51e
//
// Generated by Microsoft (R) HLSL Shader Compiler 10.1
//
//
// Buffer Definitions: 
//
// cbuffer PerDraw
// {
//
//   float4x4 World_id31;               // Offset:    0 Size:    64
//   float4x4 WorldInverse_id32;        // Offset:   64 Size:    64 [unused]
//   float4x4 WorldInverseTranspose_id33;// Offset:  128 Size:    64
//   float4x4 WorldView_id34;           // Offset:  192 Size:    64 [unused]
//   float4x4 WorldViewInverse_id35;    // Offset:  256 Size:    64 [unused]
//   float4x4 WorldViewProjection_id36; // Offset:  320 Size:    64 [unused]
//   float3 WorldScale_id37;            // Offset:  384 Size:    12 [unused]
//   float4 EyeMS_id38;                 // Offset:  400 Size:    16 [unused]
//
// }
//
// cbuffer PerView
// {
//
//   float4x4 View_id24;                // Offset:    0 Size:    64 [unused]
//   float4x4 ViewInverse_id25;         // Offset:   64 Size:    64 [unused]
//   float4x4 Projection_id26;          // Offset:  128 Size:    64 [unused]
//   float4x4 ProjectionInverse_id27;   // Offset:  192 Size:    64 [unused]
//   float4x4 ViewProjection_id28;      // Offset:  256 Size:    64
//   float2 ProjScreenRay_id29;         // Offset:  320 Size:     8 [unused]
//   float4 Eye_id30;                   // Offset:  336 Size:    16 [unused]
//   float NearClipPlane_id84;          // Offset:  352 Size:     4 [unused]
//      = 0x3f800000 
//   float FarClipPlane_id85;           // Offset:  356 Size:     4 [unused]
//      = 0x42c80000 
//   float2 ZProjection_id86;           // Offset:  360 Size:     8 [unused]
//   float2 ViewSize_id87;              // Offset:  368 Size:     8 [unused]
//   float AspectRatio_id88;            // Offset:  376 Size:     4 [unused]
//   float4 _padding_PerView_Default;   // Offset:  384 Size:    16 [unused]
//   float ClusterDepthScale_id93;      // Offset:  400 Size:     4 [unused]
//   float ClusterDepthBias_id94;       // Offset:  404 Size:     4 [unused]
//   float2 ClusterStride_id95;         // Offset:  408 Size:     8 [unused]
//   float3 AmbientLight_id98;          // Offset:  416 Size:    12 [unused]
//   float4 _padding_PerView_Lighting;  // Offset:  432 Size:    16 [unused]
//
// }
//
//
// Resource Bindings:
//
// Name                                 Type  Format         Dim      HLSL Bind  Count
// ------------------------------ ---------- ------- ----------- -------------- ------
// PerDraw                           cbuffer      NA          NA            cb0      1 
// PerView                           cbuffer      NA          NA            cb1      1 
//
//
//
// Input signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// POSITION                 0   xyzw        0     NONE   float   xyzw
// NORMAL                   0   xyz         1     NONE   float   xyz 
//
//
// Output signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// POSITION_WS              0   xyzw        0     NONE   float   xyzw
// SV_Position              0   xyzw        1      POS   float   xyzw
// NORMALWS                 0   xyz         2     NONE   float   xyz 
// SCREENPOSITION_ID83_SEM     0   xyzw        3     NONE   float   xyzw
//
vs_4_0
dcl_constantbuffer CB0[11], immediateIndexed
dcl_constantbuffer CB1[20], immediateIndexed
dcl_input v0.xyzw
dcl_input v1.xyz
dcl_output o0.xyzw
dcl_output_siv o1.xyzw, position
dcl_output o2.xyz
dcl_output o3.xyzw
dcl_temps 2
//
// Initial variable locations:
//   v0.x <- __input__.Position_id20.x; v0.y <- __input__.Position_id20.y; v0.z <- __input__.Position_id20.z; v0.w <- __input__.Position_id20.w; 
//   v1.x <- __input__.meshNormal_id15.x; v1.y <- __input__.meshNormal_id15.y; v1.z <- __input__.meshNormal_id15.z; 
//   o3.x <- <VSMain return value>.ScreenPosition_id83.x; o3.y <- <VSMain return value>.ScreenPosition_id83.y; o3.z <- <VSMain return value>.ScreenPosition_id83.z; o3.w <- <VSMain return value>.ScreenPosition_id83.w; 
//   o2.x <- <VSMain return value>.normalWS_id18.x; o2.y <- <VSMain return value>.normalWS_id18.y; o2.z <- <VSMain return value>.normalWS_id18.z; 
//   o1.x <- <VSMain return value>.ShadingPosition_id0.x; o1.y <- <VSMain return value>.ShadingPosition_id0.y; o1.z <- <VSMain return value>.ShadingPosition_id0.z; o1.w <- <VSMain return value>.ShadingPosition_id0.w; 
//   o0.x <- <VSMain return value>.PositionWS_id21.x; o0.y <- <VSMain return value>.PositionWS_id21.y; o0.z <- <VSMain return value>.PositionWS_id21.z; o0.w <- <VSMain return value>.PositionWS_id21.w
//
#line 761 "C:\Users\minor\Documents\Xenko Projects\TCGClient\TCGClient\Bin\Windows\Debug\log\shader_XenkoForwardShadingEffect_ada579464f95ee5d97abc7ee34b6ffcd.hlsl"
dp4 r0.x, v0.xyzw, cb0[0].xyzw  // r0.x <- streams.PositionWS_id21.x
dp4 r0.y, v0.xyzw, cb0[1].xyzw  // r0.y <- streams.PositionWS_id21.y
dp4 r0.z, v0.xyzw, cb0[2].xyzw  // r0.z <- streams.PositionWS_id21.z
dp4 r0.w, v0.xyzw, cb0[3].xyzw  // r0.w <- streams.PositionWS_id21.w

#line 840
mov o0.xyzw, r0.xyzw

#line 716
dp4 r1.x, r0.xyzw, cb1[16].xyzw  // r1.x <- <ComputeShadingPosition_id11 return value>.x
dp4 r1.y, r0.xyzw, cb1[17].xyzw  // r1.y <- <ComputeShadingPosition_id11 return value>.y
dp4 r1.z, r0.xyzw, cb1[18].xyzw  // r1.z <- <ComputeShadingPosition_id11 return value>.z
dp4 r1.w, r0.xyzw, cb1[19].xyzw  // r1.w <- <ComputeShadingPosition_id11 return value>.w

#line 840
mov o1.xyzw, r1.xyzw
mov o3.xyzw, r1.xyzw

#line 796
dp3 o2.x, v1.xyzx, cb0[8].xyzx
dp3 o2.y, v1.xyzx, cb0[9].xyzx
dp3 o2.z, v1.xyzx, cb0[10].xyzx

#line 840
ret 
// Approximately 15 instruction slots used
@G    Pixel => 4970211153e8ce7dd91c5607a1baae36
//
// Generated by Microsoft (R) HLSL Shader Compiler 10.1
//
//
// Buffer Definitions: 
//
// cbuffer PerMaterial
// {
//
//   float4 constantColor_id99;         // Offset:    0 Size:    16
//   float constantFloat_id101;         // Offset:   16 Size:     4
//   float constantFloat_id104;         // Offset:   20 Size:     4
//
// }
//
// cbuffer PerView
// {
//
//   float4x4 View_id24;                // Offset:    0 Size:    64 [unused]
//   float4x4 ViewInverse_id25;         // Offset:   64 Size:    64 [unused]
//   float4x4 Projection_id26;          // Offset:  128 Size:    64 [unused]
//   float4x4 ProjectionInverse_id27;   // Offset:  192 Size:    64 [unused]
//   float4x4 ViewProjection_id28;      // Offset:  256 Size:    64 [unused]
//   float2 ProjScreenRay_id29;         // Offset:  320 Size:     8 [unused]
//   float4 Eye_id30;                   // Offset:  336 Size:    16
//   float NearClipPlane_id84;          // Offset:  352 Size:     4 [unused]
//      = 0x3f800000 
//   float FarClipPlane_id85;           // Offset:  356 Size:     4 [unused]
//      = 0x42c80000 
//   float2 ZProjection_id86;           // Offset:  360 Size:     8
//   float2 ViewSize_id87;              // Offset:  368 Size:     8 [unused]
//   float AspectRatio_id88;            // Offset:  376 Size:     4 [unused]
//   float4 _padding_PerView_Default;   // Offset:  384 Size:    16 [unused]
//   float ClusterDepthScale_id93;      // Offset:  400 Size:     4
//   float ClusterDepthBias_id94;       // Offset:  404 Size:     4
//   float2 ClusterStride_id95;         // Offset:  408 Size:     8
//   float3 AmbientLight_id98;          // Offset:  416 Size:    12
//   float4 _padding_PerView_Lighting;  // Offset:  432 Size:    16 [unused]
//
// }
//
//
// Resource Bindings:
//
// Name                                 Type  Format         Dim      HLSL Bind  Count
// ------------------------------ ---------- ------- ----------- -------------- ------
// LinearSampler_id141               sampler      NA          NA             s0      1 
// EnvironmentLightingDFG_LUT_id169    texture  float4          2d             t0      1 
// LightClusters_id91                texture   uint2          3d             t1      1 
// LightIndices_id92                 texture    uint         buf             t2      1 
// PointLights_id96                  texture  float4         buf             t3      1 
// SpotLights_id97                   texture  float4         buf             t4      1 
// PerMaterial                       cbuffer      NA          NA            cb0      1 
// PerView                           cbuffer      NA          NA            cb1      1 
//
//
//
// Input signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// POSITION_WS              0   xyzw        0     NONE   float   xyz 
// SV_Position              0   xyzw        1      POS   float     z 
// NORMALWS                 0   xyz         2     NONE   float   xyz 
// SCREENPOSITION_ID83_SEM     0   xyzw        3     NONE   float   xy w
// SV_IsFrontFace           0   x           4    FFACE    uint   x   
//
//
// Output signature:
//
// Name                 Index   Mask Register SysValue  Format   Used
// -------------------- ----- ------ -------- -------- ------- ------
// SV_Target                0   xyzw        0   TARGET   float   xyzw
//
ps_4_0
dcl_constantbuffer CB0[2], immediateIndexed
dcl_constantbuffer CB1[27], immediateIndexed
dcl_sampler s0, mode_default
dcl_resource_texture2d (float,float,float,float) t0
dcl_resource_texture3d (uint,uint,uint,uint) t1
dcl_resource_buffer (uint,uint,uint,uint) t2
dcl_resource_buffer (float,float,float,float) t3
dcl_resource_buffer (float,float,float,float) t4
dcl_input_ps linear v0.xyz
dcl_input_ps_siv linear noperspective v1.z, position
dcl_input_ps linear v2.xyz
dcl_input_ps linear v3.xyw
dcl_input_ps_sgv constant v4.x, is_front_face
dcl_output o0.xyzw
dcl_temps 19
//
// Initial variable locations:
//   v0.x <- __input__.PositionWS_id21.x; v0.y <- __input__.PositionWS_id21.y; v0.z <- __input__.PositionWS_id21.z; v0.w <- __input__.PositionWS_id21.w; 
//   v1.x <- __input__.ShadingPosition_id0.x; v1.y <- __input__.ShadingPosition_id0.y; v1.z <- __input__.ShadingPosition_id0.z; v1.w <- __input__.ShadingPosition_id0.w; 
//   v2.x <- __input__.normalWS_id18.x; v2.y <- __input__.normalWS_id18.y; v2.z <- __input__.normalWS_id18.z; 
//   v3.x <- __input__.ScreenPosition_id83.x; v3.y <- __input__.ScreenPosition_id83.y; v3.z <- __input__.ScreenPosition_id83.z; v3.w <- __input__.ScreenPosition_id83.w; 
//   v4.x <- __input__.IsFrontFace_id1; 
//   o0.x <- <PSMain return value>.ColorTarget_id2.x; o0.y <- <PSMain return value>.ColorTarget_id2.y; o0.z <- <PSMain return value>.ColorTarget_id2.z; o0.w <- <PSMain return value>.ColorTarget_id2.w
//
#line 822 "C:\Users\minor\Documents\Xenko Projects\TCGClient\TCGClient\Bin\Windows\Debug\log\shader_XenkoForwardShadingEffect_ada579464f95ee5d97abc7ee34b6ffcd.hlsl"
div r0.xy, v3.xyxx, v3.wwww  // r0.x <- streams.ScreenPosition_id83.x; r0.y <- streams.ScreenPosition_id83.y

#line 790
dp3 r0.z, v2.xyzx, v2.xyzx
lt r0.w, l(0.000000), r0.z

#line 791
rsq r0.z, r0.z
mul r1.xyz, r0.zzzz, v2.xyzx  // r1.x <- streams.normalWS_id18.x; r1.y <- streams.normalWS_id18.y; r1.z <- streams.normalWS_id18.z
movc r1.xyz, r0.wwww, r1.xyzx, v2.xyzx

#line 765
add r2.xyz, -v0.xyzx, cb1[21].xyzx
dp3 r0.z, r2.xyzx, r2.xyzx
rsq r0.z, r0.z
mul r2.xyz, r0.zzzz, r2.xyzx  // r2.x <- streams.viewWS_id66.x; r2.y <- streams.viewWS_id66.y; r2.z <- streams.viewWS_id66.z

#line 676
add r3.xyz, cb0[0].xyzx, l(-0.020000, -0.020000, -0.020000, 0.000000)
mad r3.xyz, cb0[1].yyyy, r3.xyzx, l(0.020000, 0.020000, 0.020000, 0.000000)  // r3.x <- streams.matSpecular_id53.x; r3.y <- streams.matSpecular_id53.y; r3.z <- streams.matSpecular_id53.z

#line 677
mad r4.xyz, cb0[1].yyyy, -cb0[0].xyzx, cb0[0].xyzx  // r4.x <- streams.matDiffuse_id51.x; r4.y <- streams.matDiffuse_id51.y; r4.z <- streams.matDiffuse_id51.z

#line 577
movc r1.xyz, v4.xxxx, r1.xyzx, -r1.xyzx  // r1.x <- streams.normalWS_id18.x; r1.y <- streams.normalWS_id18.y; r1.z <- streams.normalWS_id18.z

#line 537
dp3 r0.z, r1.xyzx, r2.xyzx
max r5.y, r0.z, l(0.000100)  // r5.y <- streams.NdotV_id70

#line 538
add r0.z, -cb0[1].x, l(1.000000)  // r0.z <- roughness

#line 539
mul r0.z, r0.z, r0.z

#line 423
add r0.w, v1.z, -cb1[22].z
div r0.w, cb1[22].w, r0.w  // r0.w <- depth

#line 424
mad r0.xy, r0.xyxx, l(1.000000, -1.000000, 0.000000, 0.000000), l(1.000000, 1.000000, 0.000000, 0.000000)
mul r0.xy, r0.xyxx, cb1[25].zwzz

#line 425
mad r0.w, r0.w, cb1[25].x, cb1[25].y
log r0.w, r0.w

#line 539
max r0.zw, r0.zzzw, l(0.000000, 0.000000, 0.001000, 0.000000)  // r0.z <- streams.alphaRoughness_id68

#line 426
mul r0.xy, r0.xyxx, l(0.500000, 0.500000, 0.000000, 0.000000)

#line 425
ftoi r6.xyz, r0.xywx  // r6.z <- slice

#line 426
mov r6.w, l(0)
ld r6.xyzw, r6.xyzw, t1.xyzw  // r6.x <- streams.lightData_id89.x; r6.y <- streams.lightData_id89.y

#line 520
and r0.x, r6.y, l(0x0000ffff)  // r0.x <- <GetMaxLightCount_id161 return value>

#line 595
mov r7.xyz, r1.xyzx  // r7.x <- streams.normalWS_id18.x; r7.y <- streams.normalWS_id18.y; r7.z <- streams.normalWS_id18.z
mov r7.w, v0.x  // r7.w <- streams.PositionWS_id21.x
mov r8.xy, v0.yzyy  // r8.x <- streams.PositionWS_id21.y; r8.y <- streams.PositionWS_id21.z
mov r8.zw, r2.xxxy  // r8.z <- streams.viewWS_id66.x; r8.w <- streams.viewWS_id66.y
mov r9.z, r2.z  // r9.z <- streams.viewWS_id66.z
mov r10.xyz, r4.xyzx  // r10.x <- streams.matDiffuseVisible_id67.x; r10.y <- streams.matDiffuseVisible_id67.y; r10.z <- streams.matDiffuseVisible_id67.z
mov r11.xyz, r3.xyzx  // r11.x <- streams.matSpecularVisible_id69.x; r11.y <- streams.matSpecularVisible_id69.y; r11.z <- streams.matSpecularVisible_id69.z
mov r11.w, r0.z  // r11.w <- streams.alphaRoughness_id68
mov r0.w, r5.y  // r0.w <- streams.NdotV_id70
mov r0.y, r6.x  // r0.y <- streams.lightIndex_id90
mov r12.xyz, l(0,0,0,0)  // r12.x <- directLightingContribution.x; r12.y <- directLightingContribution.y; r12.z <- directLightingContribution.z
mov r1.w, l(0)  // r1.w <- i
loop 
  ige r2.w, r1.w, r0.x
  breakc_nz r2.w

#line 597
  if_nz r2.w

#line 599
    break 

#line 600
  endif 

#line 410
  ld r13.xyzw, r0.yyyy, t2.xyzw  // r13.x <- realLightIndex

#line 411
  iadd r0.y, r0.y, l(1)  // r0.y <- streams.lightIndex_id90

#line 413
  ishl r2.w, r13.x, l(1)
  ld r13.xyzw, r2.wwww, t3.xyzw  // r13.x <- pointLight1.x; r13.y <- pointLight1.y; r13.z <- pointLight1.z; r13.w <- pointLight1.w

#line 414
  iadd r2.w, r2.w, l(1)
  ld r14.xyzw, r2.wwww, t3.xyzw  // r14.x <- pointLight2.x; r14.y <- pointLight2.y; r14.z <- pointLight2.z

#line 309
  mov r15.x, r7.w
  mov r15.yz, r8.xxyx
  add r13.xyz, r13.xyzx, -r15.xyzx  // r13.x <- lightVector.x; r13.y <- lightVector.y; r13.z <- lightVector.z

#line 310
  dp3 r2.w, r13.xyzx, r13.xyzx
  sqrt r3.w, r2.w  // r3.w <- lightVectorLength

#line 311
  div r13.xyz, r13.xyzx, r3.wwww  // r13.x <- lightVectorNorm.x; r13.y <- lightVectorNorm.y; r13.z <- lightVectorNorm.z

#line 281
  max r3.w, r2.w, l(0.000100)
  div r3.w, l(1.000000, 1.000000, 1.000000, 1.000000), r3.w  // r3.w <- attenuation

#line 260
  mul r2.w, r13.w, r2.w  // r2.w <- factor

#line 261
  mad r2.w, -r2.w, r2.w, l(1.000000)
  max r2.w, r2.w, l(0.000000)  // r2.w <- smoothFactor

#line 262
  mul r2.w, r2.w, r2.w  // r2.w <- <SmoothDistanceAttenuation_id53 return value>

#line 282
  mul r2.w, r2.w, r3.w  // r2.w <- attenuation

#line 341
  mul r14.xyz, r2.wwww, r14.xyzx  // r14.x <- streams.lightColor_id42.x; r14.y <- streams.lightColor_id42.y; r14.z <- streams.lightColor_id42.z

#line 506
  dp3 r2.w, r7.xyzx, r13.xyzx
  max r2.w, r2.w, l(0.000100)  // r2.w <- streams.NdotL_id47

#line 508
  mul r14.xyz, r2.wwww, r14.xyzx  // r14.x <- streams.lightColorNdotL_id43.x; r14.y <- streams.lightColorNdotL_id43.y; r14.z <- streams.lightColorNdotL_id43.z

#line 498
  mov r9.xy, r8.zwzz
  add r9.xyw, r13.xyxz, r9.xyxz
  dp3 r3.w, r9.xywx, r9.xywx
  rsq r3.w, r3.w
  mul r9.xyw, r3.wwww, r9.xyxw  // r9.x <- streams.H_id73.x; r9.y <- streams.H_id73.y; r9.w <- streams.H_id73.z

#line 499
  dp3_sat r3.w, r7.xyzx, r9.xywx  // r3.w <- streams.NdotH_id74

#line 500
  dp3_sat r4.w, r13.xyzx, r9.xywx  // r4.w <- streams.LdotH_id75

#line 494
  mul r9.xyw, r10.xyxz, r14.xyxz

#line 605
  mad r9.xyw, r9.xyxw, l(0.318310, 0.318310, 0.000000, 0.318310), r12.xyxz  // r9.x <- directLightingContribution.x; r9.y <- directLightingContribution.y; r9.w <- directLightingContribution.z

#line 305
  add r13.xyz, -r11.xyzx, l(1.000000, 1.000000, 1.000000, 0.000000)
  add r4.w, -r4.w, l(1.000000)
  mul r5.z, r4.w, r4.w
  mul r5.z, r5.z, r5.z
  mul r4.w, r4.w, r5.z
  mad r13.xyz, r13.xyzx, r4.wwww, r11.xyzx  // r13.x <- <FresnelSchlick_id114 return value>.x; r13.y <- <FresnelSchlick_id114 return value>.y; r13.z <- <FresnelSchlick_id114 return value>.z

#line 300
  mul r4.w, r11.w, l(0.500000)  // r4.w <- k

#line 301
  mad r5.z, -r11.w, l(0.500000), l(1.000000)
  mad r5.w, r2.w, r5.z, r4.w
  div r5.w, r2.w, r5.w  // r5.w <- <VisibilityhSchlickGGX_id130 return value>
  mad r4.w, r0.w, r5.z, r4.w
  div r4.w, r0.w, r4.w  // r4.w <- <VisibilityhSchlickGGX_id130 return value>

#line 330
  mul r4.w, r4.w, r5.w
  mul r2.w, r0.w, r2.w
  div r2.w, r4.w, r2.w  // r2.w <- <VisibilitySmithSchlickGGX_id132 return value>

#line 324
  mul r4.w, r11.w, r11.w  // r4.w <- alphaR2

#line 325
  mul r3.w, r3.w, r3.w
  mad r5.z, r11.w, r11.w, l(-1.000000)
  mad r3.w, r3.w, r5.z, l(1.000000)
  max r3.w, r3.w, l(0.000100)  // r3.w <- d

#line 326
  mul r3.w, r3.w, r3.w
  mul r3.w, r3.w, l(3.141593)
  div r3.w, r4.w, r3.w  // r3.w <- <NormalDistributionGGX_id144 return value>

#line 488
  mul r13.xyz, r2.wwww, r13.xyzx
  mul r13.xyz, r3.wwww, r13.xyzx
  mul r13.xyz, r14.xyzx, r13.xyzx

#line 609
  mad r12.xyz, r13.xyzx, l(0.250000, 0.250000, 0.250000, 0.000000), r9.xywx  // r12.x <- directLightingContribution.x; r12.y <- directLightingContribution.y; r12.z <- directLightingContribution.z

#line 595
  iadd r1.w, r1.w, l(1)

#line 611
endloop   // r0.y <- streams.lightIndex_id90

#line 477
ushr r0.x, r6.y, l(16)  // r0.x <- <GetMaxLightCount_id173 return value>

#line 619
mov r6.xyz, r1.xyzx  // r6.x <- streams.normalWS_id18.x; r6.y <- streams.normalWS_id18.y; r6.z <- streams.normalWS_id18.z
mov r6.w, v0.x  // r6.w <- streams.PositionWS_id21.x
mov r7.xy, v0.yzyy  // r7.x <- streams.PositionWS_id21.y; r7.y <- streams.PositionWS_id21.z
mov r7.zw, r2.xxxy  // r7.z <- streams.viewWS_id66.x; r7.w <- streams.viewWS_id66.y
mov r8.z, r2.z  // r8.z <- streams.viewWS_id66.z
mov r9.xyz, r4.xyzx  // r9.x <- streams.matDiffuseVisible_id67.x; r9.y <- streams.matDiffuseVisible_id67.y; r9.z <- streams.matDiffuseVisible_id67.z
mov r10.xyz, r3.xyzx  // r10.x <- streams.matSpecularVisible_id69.x; r10.y <- streams.matSpecularVisible_id69.y; r10.z <- streams.matSpecularVisible_id69.z
mov r11.x, r0.z  // r11.x <- streams.alphaRoughness_id68
mov r11.z, r5.y  // r11.z <- streams.NdotV_id70
mov r13.xyz, r12.xyzx  // r13.x <- directLightingContribution.x; r13.y <- directLightingContribution.y; r13.z <- directLightingContribution.z
mov r0.w, r0.y  // r0.w <- streams.lightIndex_id90
mov r1.w, l(0)  // r1.w <- i
loop 
  ige r2.w, r1.w, r0.x
  breakc_nz r2.w

#line 621
  if_nz r2.w

#line 623
    break 

#line 624
  endif 

#line 370
  ld r14.xyzw, r0.wwww, t2.xyzw  // r14.x <- realLightIndex

#line 371
  iadd r0.w, r0.w, l(1)  // r0.w <- streams.lightIndex_id90

#line 373
  ishl r2.w, r14.x, l(2)
  ld r14.xyzw, r2.wwww, t4.xyzw  // r14.x <- spotLight1.x; r14.y <- spotLight1.y; r14.z <- spotLight1.z

#line 376
  iadd r15.xyz, r2.wwww, l(1, 2, 3, 0)

#line 374
  ld r16.xyzw, r15.xxxx, t4.xyzw  // r16.x <- spotLight2.x; r16.y <- spotLight2.y; r16.z <- spotLight2.z

#line 375
  ld r17.xyzw, r15.yyyy, t4.xyzw  // r17.x <- spotLight3.x; r17.y <- spotLight3.y; r17.z <- spotLight3.z

#line 376
  ld r15.xyzw, r15.zzzz, t4.xyzw  // r15.x <- spotLight4.x; r15.y <- spotLight4.y; r15.z <- spotLight4.z

#line 287
  mov r18.x, r6.w
  mov r18.yz, r7.xxyx
  add r14.xyz, r14.xyzx, -r18.xyzx  // r14.x <- lightVector.x; r14.y <- lightVector.y; r14.z <- lightVector.z

#line 288
  dp3 r2.w, r14.xyzx, r14.xyzx
  sqrt r3.w, r2.w  // r3.w <- lightVectorLength

#line 289
  div r14.xyz, r14.xyzx, r3.wwww  // r14.x <- lightVectorNorm.x; r14.y <- lightVectorNorm.y; r14.z <- lightVectorNorm.z

#line 274
  max r3.w, r2.w, l(0.000100)
  div r3.w, l(1.000000, 1.000000, 1.000000, 1.000000), r3.w  // r3.w <- attenuation

#line 254
  mul r2.w, r17.z, r2.w  // r2.w <- factor

#line 255
  mad r2.w, -r2.w, r2.w, l(1.000000)
  max r2.w, r2.w, l(0.000000)  // r2.w <- smoothFactor

#line 256
  mul r2.w, r2.w, r2.w  // r2.w <- <SmoothDistanceAttenuation_id64 return value>

#line 275
  mul r2.w, r2.w, r3.w  // r2.w <- attenuation

#line 266
  dp3 r3.w, -r16.xyzx, r14.xyzx  // r3.w <- cd

#line 267
  mad_sat r3.w, r3.w, r17.x, r17.y  // r3.w <- attenuation

#line 268
  mul r3.w, r3.w, r3.w

#line 295
  mul r2.w, r2.w, r3.w  // r2.w <- attenuation

#line 319
  mul r15.xyz, r2.wwww, r15.xyzx  // r15.x <- streams.lightColor_id42.x; r15.y <- streams.lightColor_id42.y; r15.z <- streams.lightColor_id42.z

#line 463
  dp3 r2.w, r6.xyzx, r14.xyzx
  max r2.w, r2.w, l(0.000100)  // r2.w <- streams.NdotL_id47

#line 465
  mul r15.xyz, r2.wwww, r15.xyzx  // r15.x <- streams.lightColorNdotL_id43.x; r15.y <- streams.lightColorNdotL_id43.y; r15.z <- streams.lightColorNdotL_id43.z

#line 498
  mov r8.xy, r7.zwzz
  add r8.xyw, r14.xyxz, r8.xyxz
  dp3 r3.w, r8.xywx, r8.xywx
  rsq r3.w, r3.w
  mul r8.xyw, r3.wwww, r8.xyxw  // r8.x <- streams.H_id73.x; r8.y <- streams.H_id73.y; r8.w <- streams.H_id73.z

#line 499
  dp3_sat r3.w, r6.xyzx, r8.xywx  // r3.w <- streams.NdotH_id74

#line 500
  dp3_sat r4.w, r14.xyzx, r8.xywx  // r4.w <- streams.LdotH_id75

#line 494
  mul r8.xyw, r9.xyxz, r15.xyxz

#line 629
  mad r8.xyw, r8.xyxw, l(0.318310, 0.318310, 0.000000, 0.318310), r13.xyxz  // r8.x <- directLightingContribution.x; r8.y <- directLightingContribution.y; r8.w <- directLightingContribution.z

#line 305
  add r14.xyz, -r10.xyzx, l(1.000000, 1.000000, 1.000000, 0.000000)
  add r4.w, -r4.w, l(1.000000)
  mul r5.z, r4.w, r4.w
  mul r5.z, r5.z, r5.z
  mul r4.w, r4.w, r5.z
  mad r14.xyz, r14.xyzx, r4.wwww, r10.xyzx  // r14.x <- <FresnelSchlick_id114 return value>.x; r14.y <- <FresnelSchlick_id114 return value>.y; r14.z <- <FresnelSchlick_id114 return value>.z

#line 300
  mul r4.w, r11.x, l(0.500000)  // r4.w <- k

#line 301
  mad r5.z, -r11.x, l(0.500000), l(1.000000)
  mad r5.w, r2.w, r5.z, r4.w
  div r5.w, r2.w, r5.w  // r5.w <- <VisibilityhSchlickGGX_id130 return value>
  mad r4.w, r11.z, r5.z, r4.w
  div r4.w, r11.z, r4.w  // r4.w <- <VisibilityhSchlickGGX_id130 return value>

#line 330
  mul r4.w, r4.w, r5.w
  mul r2.w, r11.z, r2.w
  div r2.w, r4.w, r2.w  // r2.w <- <VisibilitySmithSchlickGGX_id132 return value>

#line 324
  mul r4.w, r11.x, r11.x  // r4.w <- alphaR2

#line 325
  mul r3.w, r3.w, r3.w
  mad r5.z, r11.x, r11.x, l(-1.000000)
  mad r3.w, r3.w, r5.z, l(1.000000)
  max r3.w, r3.w, l(0.000100)  // r3.w <- d

#line 326
  mul r3.w, r3.w, r3.w
  mul r3.w, r3.w, l(3.141593)
  div r3.w, r4.w, r3.w  // r3.w <- <NormalDistributionGGX_id144 return value>

#line 488
  mul r14.xyz, r2.wwww, r14.xyzx
  mul r14.xyz, r3.wwww, r14.xyzx
  mul r14.xyz, r15.xyzx, r14.xyzx

#line 633
  mad r13.xyz, r14.xyzx, l(0.250000, 0.250000, 0.250000, 0.000000), r8.xywx  // r13.x <- directLightingContribution.x; r13.y <- directLightingContribution.y; r13.z <- directLightingContribution.z

#line 619
  iadd r1.w, r1.w, l(1)

#line 635
endloop   // r0.w <- streams.lightIndex_id90

#line 346
sqrt r0.x, r0.z
add r5.x, -r0.x, l(1.000000)  // r5.x <- glossiness

#line 347
sample_l r0.xyzw, r5.xyxx, t0.xyzw, s0, l(0.000000)  // r0.x <- environmentLightingDFG.x; r0.y <- environmentLightingDFG.y

#line 348
mad r0.xyz, r3.xyzx, r0.xxxx, r0.yyyy  // r0.x <- <Compute_id189 return value>.x; r0.y <- <Compute_id189 return value>.y; r0.z <- <Compute_id189 return value>.z

#line 446
mul r0.xyz, r0.xyzx, cb1[26].xyzx  // r0.x <- <ComputeEnvironmentLightContribution_id368 return value>.x; r0.y <- <ComputeEnvironmentLightContribution_id368 return value>.y; r0.z <- <ComputeEnvironmentLightContribution_id368 return value>.z

#line 647
mad r0.xyz, r4.xyzx, cb1[26].xyzx, r0.xyzx  // r0.x <- environmentLightingContribution.x; r0.y <- environmentLightingContribution.y; r0.z <- environmentLightingContribution.z

#line 662
mad o0.xyz, r13.xyzx, l(3.141593, 3.141593, 3.141593, 0.000000), r0.xyzx

#line 826
mov o0.w, cb0[0].w
ret 
// Approximately 204 instruction slots used
***************************
*************************/
const static bool TInvert_id102 = false;
const static bool TIsEnergyConservative_id108 = false;
static const float PI_id110 = 3.14159265358979323846;
struct PS_STREAMS 
{
    float4 ScreenPosition_id83;
    float3 normalWS_id18;
    float4 PositionWS_id21;
    float4 ShadingPosition_id0;
    bool IsFrontFace_id1;
    float3 meshNormalWS_id16;
    float3 viewWS_id66;
    float3 shadingColor_id71;
    float matBlend_id39;
    float3 matNormal_id49;
    float4 matColorBase_id50;
    float4 matDiffuse_id51;
    float3 matDiffuseVisible_id67;
    float3 matSpecular_id53;
    float3 matSpecularVisible_id69;
    float matSpecularIntensity_id54;
    float matGlossiness_id52;
    float alphaRoughness_id68;
    float matAmbientOcclusion_id55;
    float matAmbientOcclusionDirectLightingFactor_id56;
    float matCavity_id57;
    float matCavityDiffuse_id58;
    float matCavitySpecular_id59;
    float4 matEmissive_id60;
    float matEmissiveIntensity_id61;
    float matScatteringStrength_id62;
    float2 matDiffuseSpecularAlphaBlend_id63;
    float3 matAlphaBlendColor_id64;
    float matAlphaDiscard_id65;
    float shadingColorAlpha_id72;
    float3 lightPositionWS_id40;
    float3 lightDirectionWS_id41;
    float3 lightColor_id42;
    float3 lightColorNdotL_id43;
    float3 lightSpecularColorNdotL_id44;
    float3 envLightDiffuseColor_id45;
    float3 envLightSpecularColor_id46;
    float lightDirectAmbientOcclusion_id48;
    float NdotL_id47;
    float NdotV_id70;
    uint2 lightData_id89;
    int lightIndex_id90;
    float thicknessWS_id82;
    float3 shadowColor_id81;
    float3 H_id73;
    float NdotH_id74;
    float LdotH_id75;
    float VdotH_id76;
    float4 ColorTarget_id2;
};
struct PS_OUTPUT 
{
    float4 ColorTarget_id2 : SV_Target0;
};
struct PS_INPUT 
{
    float4 PositionWS_id21 : POSITION_WS;
    float4 ShadingPosition_id0 : SV_Position;
    float3 normalWS_id18 : NORMALWS;
    float4 ScreenPosition_id83 : SCREENPOSITION_ID83_SEM;
    bool IsFrontFace_id1 : SV_IsFrontFace;
};
struct VS_STREAMS 
{
    float4 Position_id20;
    float3 meshNormal_id15;
    float4 PositionH_id23;
    float DepthVS_id22;
    float3 meshNormalWS_id16;
    float4 PositionWS_id21;
    float4 ShadingPosition_id0;
    float3 normalWS_id18;
    float4 ScreenPosition_id83;
};
struct VS_OUTPUT 
{
    float4 PositionWS_id21 : POSITION_WS;
    float4 ShadingPosition_id0 : SV_Position;
    float3 normalWS_id18 : NORMALWS;
    float4 ScreenPosition_id83 : SCREENPOSITION_ID83_SEM;
};
struct VS_INPUT 
{
    float4 Position_id20 : POSITION;
    float3 meshNormal_id15 : NORMAL;
};
struct PointLightData 
{
    float3 PositionWS;
    float InvSquareRadius;
    float3 Color;
};
struct PointLightDataInternal 
{
    float3 PositionWS;
    float InvSquareRadius;
    float3 Color;
};
struct SpotLightDataInternal 
{
    float3 PositionWS;
    float3 DirectionWS;
    float3 AngleOffsetAndInvSquareRadius;
    float3 Color;
};
struct SpotLightData 
{
    float3 PositionWS;
    float3 DirectionWS;
    float3 AngleOffsetAndInvSquareRadius;
    float3 Color;
};
cbuffer PerDraw 
{
    float4x4 World_id31;
    float4x4 WorldInverse_id32;
    float4x4 WorldInverseTranspose_id33;
    float4x4 WorldView_id34;
    float4x4 WorldViewInverse_id35;
    float4x4 WorldViewProjection_id36;
    float3 WorldScale_id37;
    float4 EyeMS_id38;
};
cbuffer PerMaterial 
{
    float4 constantColor_id99;
    float constantFloat_id101;
    float constantFloat_id104;
};
cbuffer PerView 
{
    float4x4 View_id24;
    float4x4 ViewInverse_id25;
    float4x4 Projection_id26;
    float4x4 ProjectionInverse_id27;
    float4x4 ViewProjection_id28;
    float2 ProjScreenRay_id29;
    float4 Eye_id30;
    float NearClipPlane_id84 = 1.0f;
    float FarClipPlane_id85 = 100.0f;
    float2 ZProjection_id86;
    float2 ViewSize_id87;
    float AspectRatio_id88;
    float4 _padding_PerView_Default;
    float ClusterDepthScale_id93;
    float ClusterDepthBias_id94;
    float2 ClusterStride_id95;
    float3 AmbientLight_id98;
    float4 _padding_PerView_Lighting;
};
cbuffer Globals 
{
    float2 Texture0TexelSize_id112;
    float2 Texture1TexelSize_id114;
    float2 Texture2TexelSize_id116;
    float2 Texture3TexelSize_id118;
    float2 Texture4TexelSize_id120;
    float2 Texture5TexelSize_id122;
    float2 Texture6TexelSize_id124;
    float2 Texture7TexelSize_id126;
    float2 Texture8TexelSize_id128;
    float2 Texture9TexelSize_id130;
};
Texture2D Texture0_id111;
Texture2D Texture1_id113;
Texture2D Texture2_id115;
Texture2D Texture3_id117;
Texture2D Texture4_id119;
Texture2D Texture5_id121;
Texture2D Texture6_id123;
Texture2D Texture7_id125;
Texture2D Texture8_id127;
Texture2D Texture9_id129;
TextureCube TextureCube0_id131;
TextureCube TextureCube1_id132;
TextureCube TextureCube2_id133;
TextureCube TextureCube3_id134;
Texture3D Texture3D0_id135;
Texture3D Texture3D1_id136;
Texture3D Texture3D2_id137;
Texture3D Texture3D3_id138;
SamplerState Sampler_id139;
SamplerState PointSampler_id140 
{
    Filter = MIN_MAG_MIP_POINT;
};
SamplerState LinearSampler_id141 
{
    Filter = MIN_MAG_MIP_LINEAR;
};
SamplerState LinearBorderSampler_id142 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Border;
    AddressV = Border;
};
SamplerComparisonState LinearClampCompareLessEqualSampler_id143 
{
    Filter = COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
    ComparisonFunc = LessEqual;
};
SamplerState AnisotropicSampler_id144 
{
    Filter = ANISOTROPIC;
};
SamplerState AnisotropicRepeatSampler_id145 
{
    Filter = ANISOTROPIC;
    AddressU = Wrap;
    AddressV = Wrap;
    MaxAnisotropy = 16;
};
SamplerState PointRepeatSampler_id146 
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState LinearRepeatSampler_id147 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState RepeatSampler_id148 
{
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState Sampler0_id149;
SamplerState Sampler1_id150;
SamplerState Sampler2_id151;
SamplerState Sampler3_id152;
SamplerState Sampler4_id153;
SamplerState Sampler5_id154;
SamplerState Sampler6_id155;
SamplerState Sampler7_id156;
SamplerState Sampler8_id157;
SamplerState Sampler9_id158;
Texture2D EnvironmentLightingDFG_LUT_id169;
Texture3D<uint2> LightClusters_id91;
Buffer<uint> LightIndices_id92;
Buffer<float4> PointLights_id96;
Buffer<float4> SpotLights_id97;
float SmoothDistanceAttenuation_id64(float squaredDistance, float lightInvSquareRadius)
{
    float factor = squaredDistance * lightInvSquareRadius;
    float smoothFactor = saturate(1.0f - factor * factor);
    return smoothFactor * smoothFactor;
}
float SmoothDistanceAttenuation_id53(float squaredDistance, float lightInvSquareRadius)
{
    float factor = squaredDistance * lightInvSquareRadius;
    float smoothFactor = saturate(1.0f - factor * factor);
    return smoothFactor * smoothFactor;
}
float GetAngularAttenuation_id66(float3 lightVector, float3 lightDirection, float lightAngleScale, float lightAngleOffset)
{
    float cd = dot(lightDirection, lightVector);
    float attenuation = saturate(cd * lightAngleScale + lightAngleOffset);
    attenuation *= attenuation;
    return attenuation;
}
float GetDistanceAttenuation_id65(float lightVectorLength, float lightInvSquareRadius)
{
    float d2 = lightVectorLength * lightVectorLength;
    float attenuation = 1.0 / (max(d2, 0.01 * 0.01));
    attenuation *= SmoothDistanceAttenuation_id64(d2, lightInvSquareRadius);
    return attenuation;
}
float GetDistanceAttenuation_id55(float lightVectorLength, float lightInvSquareRadius)
{
    float d2 = lightVectorLength * lightVectorLength;
    float attenuation = 1.0 / (max(d2, 0.01 * 0.01));
    attenuation *= SmoothDistanceAttenuation_id53(d2, lightInvSquareRadius);
    return attenuation;
}
float ComputeAttenuation_id67(float3 PositionWS, float3 AngleOffsetAndInvSquareRadius, float3 DirectionWS, float3 position, inout float3 lightVectorNorm)
{
    float3 lightVector = PositionWS - position;
    float lightVectorLength = length(lightVector);
    lightVectorNorm = lightVector / lightVectorLength;
    float3 lightAngleOffsetAndInvSquareRadius = AngleOffsetAndInvSquareRadius;
    float2 lightAngleAndOffset = lightAngleOffsetAndInvSquareRadius.xy;
    float lightInvSquareRadius = lightAngleOffsetAndInvSquareRadius.z;
    float3 lightDirection = -DirectionWS;
    float attenuation = GetDistanceAttenuation_id65(lightVectorLength, lightInvSquareRadius);
    attenuation *= GetAngularAttenuation_id66(lightVectorNorm, lightDirection, lightAngleAndOffset.x, lightAngleAndOffset.y);
    return attenuation;
}
float VisibilityhSchlickGGX_id130(float alphaR, float nDotX)
{
    float k = alphaR * 0.5f;
    return nDotX / (nDotX * (1.0f - k) + k);
}
float3 FresnelSchlick_id114(float3 f0, float3 f90, float lOrVDotH)
{
    return f0 + (f90 - f0) * pow((1 - lOrVDotH), 5);
}
float ComputeAttenuation_id54(PointLightDataInternal light, float3 position, inout float3 lightVectorNorm)
{
    float3 lightVector = light.PositionWS - position;
    float lightVectorLength = length(lightVector);
    lightVectorNorm = lightVector / lightVectorLength;
    float lightInvSquareRadius = light.InvSquareRadius;
    return GetDistanceAttenuation_id55(lightVectorLength, lightInvSquareRadius);
}
void ProcessLight_id68(inout PS_STREAMS streams, SpotLightDataInternal light)
{
    float3 lightVectorNorm;
    float attenuation = ComputeAttenuation_id67(light.PositionWS, light.AngleOffsetAndInvSquareRadius, light.DirectionWS, streams.PositionWS_id21.xyz, lightVectorNorm);
    streams.lightColor_id42 = light.Color * attenuation;
    streams.lightDirectionWS_id41 = lightVectorNorm;
}
float NormalDistributionGGX_id144(float alphaR, float nDotH)
{
    float alphaR2 = alphaR * alphaR;
    float d = max(nDotH * nDotH * (alphaR2 - 1) + 1, 0.0001);
    return alphaR2 / (PI_id110 * d * d);
}
float VisibilitySmithSchlickGGX_id132(float alphaR, float nDotL, float nDotV)
{
    return VisibilityhSchlickGGX_id130(alphaR, nDotL) * VisibilityhSchlickGGX_id130(alphaR, nDotV) / (nDotL * nDotV);
}
float3 FresnelSchlick_id120(float3 f0, float lOrVDotH)
{
    return FresnelSchlick_id114(f0, 1.0f, lOrVDotH);
}
void ProcessLight_id57(inout PS_STREAMS streams, PointLightDataInternal light)
{
    float3 lightVectorNorm;
    float attenuation = ComputeAttenuation_id54(light, streams.PositionWS_id21.xyz, lightVectorNorm);
    streams.lightPositionWS_id40 = light.PositionWS;
    streams.lightColor_id42 = light.Color * attenuation;
    streams.lightDirectionWS_id41 = lightVectorNorm;
}
float3 Compute_id189(float3 specularColor, float alphaR, float nDotV)
{
    float glossiness = 1.0f - sqrt(alphaR);
    float4 environmentLightingDFG = EnvironmentLightingDFG_LUT_id169.SampleLevel(LinearSampler_id141, float2(glossiness, nDotV), 0);
    return specularColor * environmentLightingDFG.r + environmentLightingDFG.g;
}
void PrepareEnvironmentLight_id72(inout PS_STREAMS streams)
{
    streams.envLightDiffuseColor_id45 = 0;
    streams.envLightSpecularColor_id46 = 0;
}
float3 ComputeSpecularTextureProjection_id63(float3 positionWS, float3 reflectionWS, int lightIndex)
{
    return 1.0f;
}
float3 ComputeTextureProjection_id62(float3 positionWS, int lightIndex)
{
    return 1.0f;
}
float3 ComputeShadow_id61(inout PS_STREAMS streams, float3 position, int lightIndex)
{
    streams.thicknessWS_id82 = 0.0;
    return 1.0f;
}
void PrepareDirectLightCore_id60(inout PS_STREAMS streams, int lightIndexIgnored)
{
    int realLightIndex = LightIndices_id92.Load(streams.lightIndex_id90);
    streams.lightIndex_id90++;
    SpotLightDataInternal spotLight;
    float4 spotLight1 = SpotLights_id97.Load(realLightIndex * 4);
    float4 spotLight2 = SpotLights_id97.Load(realLightIndex * 4 + 1);
    float4 spotLight3 = SpotLights_id97.Load(realLightIndex * 4 + 2);
    float4 spotLight4 = SpotLights_id97.Load(realLightIndex * 4 + 3);
    spotLight.PositionWS = spotLight1.xyz;
    spotLight.DirectionWS = spotLight2.xyz;
    spotLight.AngleOffsetAndInvSquareRadius = spotLight3.xyz;
    spotLight.Color = spotLight4.xyz;
    ProcessLight_id68(streams, spotLight);
}
float Compute_id281(inout PS_STREAMS streams)
{
    return NormalDistributionGGX_id144(streams.alphaRoughness_id68, streams.NdotH_id74);
}
float Compute_id251(inout PS_STREAMS streams)
{
    return VisibilitySmithSchlickGGX_id132(streams.alphaRoughness_id68, streams.NdotL_id47, streams.NdotV_id70);
}
float3 Compute_id221(inout PS_STREAMS streams, float3 f0)
{
    return FresnelSchlick_id120(f0, streams.LdotH_id75);
}
float3 ComputeSpecularTextureProjection_id52(float3 positionWS, float3 reflectionWS, int lightIndex)
{
    return 1.0f;
}
float3 ComputeTextureProjection_id51(float3 positionWS, int lightIndex)
{
    return 1.0f;
}
float3 ComputeShadow_id50(inout PS_STREAMS streams, float3 position, int lightIndex)
{
    streams.thicknessWS_id82 = 0.0;
    return 1.0f;
}
void PrepareDirectLightCore_id49(inout PS_STREAMS streams, int lightIndexIgnored)
{
    int realLightIndex = LightIndices_id92.Load(streams.lightIndex_id90);
    streams.lightIndex_id90++;
    PointLightDataInternal pointLight;
    float4 pointLight1 = PointLights_id96.Load(realLightIndex * 2);
    float4 pointLight2 = PointLights_id96.Load(realLightIndex * 2 + 1);
    pointLight.PositionWS = pointLight1.xyz;
    pointLight.InvSquareRadius = pointLight1.w;
    pointLight.Color = pointLight2.xyz;
    ProcessLight_id57(streams, pointLight);
}
void PrepareLightData_id56(inout PS_STREAMS streams)
{
    float projectedDepth = streams.ShadingPosition_id0.z;
    float depth = ZProjection_id86.y / (projectedDepth - ZProjection_id86.x);
    float2 texCoord = float2(streams.ScreenPosition_id83.x + 1, 1 - streams.ScreenPosition_id83.y) * 0.5;
    int slice = int(max(log2(depth * ClusterDepthScale_id93 + ClusterDepthBias_id94), 0));
    streams.lightData_id89 = LightClusters_id91.Load(int4(texCoord * ClusterStride_id95, slice, 0));
    streams.lightIndex_id90 = streams.lightData_id89.x;
}
void ResetStream_id147()
{
}
void AfterLightingAndShading_id338()
{
}
void AfterLightingAndShading_id319()
{
}
void PrepareEnvironmentLight_id182(inout PS_STREAMS streams)
{
    streams.envLightDiffuseColor_id45 = 0;
    streams.envLightSpecularColor_id46 = 0;
}
float3 ComputeEnvironmentLightContribution_id368(inout PS_STREAMS streams)
{
    float3 specularColor = streams.matSpecularVisible_id69;
    return Compute_id189(specularColor, streams.alphaRoughness_id68, streams.NdotV_id70) * streams.envLightSpecularColor_id46;
}
float3 ComputeEnvironmentLightContribution_id312(inout PS_STREAMS streams)
{
    float3 diffuseColor = streams.matDiffuseVisible_id67;
    return diffuseColor * streams.envLightDiffuseColor_id45;
}
void PrepareEnvironmentLight_id179(inout PS_STREAMS streams)
{
    PrepareEnvironmentLight_id72(streams);
    float3 lightColor = AmbientLight_id98 * streams.matAmbientOcclusion_id55;
    streams.envLightDiffuseColor_id45 = lightColor;
    streams.envLightSpecularColor_id46 = lightColor;
}
void PrepareDirectLight_id170(inout PS_STREAMS streams, int lightIndex)
{
    PrepareDirectLightCore_id60(streams, lightIndex);
    streams.NdotL_id47 = max(dot(streams.normalWS_id18, streams.lightDirectionWS_id41), 0.0001f);
    streams.shadowColor_id81 = ComputeShadow_id61(streams, streams.PositionWS_id21.xyz, lightIndex);
    streams.lightColorNdotL_id43 = streams.lightColor_id42 * streams.shadowColor_id81 * streams.NdotL_id47 * streams.lightDirectAmbientOcclusion_id48;
    streams.lightSpecularColorNdotL_id44 = streams.lightColorNdotL_id43;
    streams.lightColorNdotL_id43 *= ComputeTextureProjection_id62(streams.PositionWS_id21.xyz, lightIndex);
    float3 reflectionVectorWS = reflect(-streams.viewWS_id66, streams.normalWS_id18);
    streams.lightSpecularColorNdotL_id44 *= ComputeSpecularTextureProjection_id63(streams.PositionWS_id21.xyz, reflectionVectorWS, lightIndex);
}
int GetLightCount_id174(inout PS_STREAMS streams)
{
    return streams.lightData_id89.y >> 16;
}
int GetMaxLightCount_id173(inout PS_STREAMS streams)
{
    return streams.lightData_id89.y >> 16;
}
void PrepareDirectLights_id168()
{
}
float3 ComputeDirectLightContribution_id367(inout PS_STREAMS streams)
{
    float3 specularColor = streams.matSpecularVisible_id69;
    float3 fresnel = Compute_id221(streams, specularColor);
    float geometricShadowing = Compute_id251(streams);
    float normalDistribution = Compute_id281(streams);
    float3 reflected = fresnel * geometricShadowing * normalDistribution / 4;
    return reflected * streams.lightSpecularColorNdotL_id44 * streams.matDiffuseSpecularAlphaBlend_id63.y;
}
float3 ComputeDirectLightContribution_id311(inout PS_STREAMS streams)
{
    float3 diffuseColor = streams.matDiffuseVisible_id67;
    return diffuseColor / PI_id110 * streams.lightColorNdotL_id43 * streams.matDiffuseSpecularAlphaBlend_id63.x;
}
void PrepareMaterialPerDirectLight_id30(inout PS_STREAMS streams)
{
    streams.H_id73 = normalize(streams.viewWS_id66 + streams.lightDirectionWS_id41);
    streams.NdotH_id74 = saturate(dot(streams.normalWS_id18, streams.H_id73));
    streams.LdotH_id75 = saturate(dot(streams.lightDirectionWS_id41, streams.H_id73));
    streams.VdotH_id76 = streams.LdotH_id75;
}
void PrepareDirectLight_id157(inout PS_STREAMS streams, int lightIndex)
{
    PrepareDirectLightCore_id49(streams, lightIndex);
    streams.NdotL_id47 = max(dot(streams.normalWS_id18, streams.lightDirectionWS_id41), 0.0001f);
    streams.shadowColor_id81 = ComputeShadow_id50(streams, streams.PositionWS_id21.xyz, lightIndex);
    streams.lightColorNdotL_id43 = streams.lightColor_id42 * streams.shadowColor_id81 * streams.NdotL_id47 * streams.lightDirectAmbientOcclusion_id48;
    streams.lightSpecularColorNdotL_id44 = streams.lightColorNdotL_id43;
    streams.lightColorNdotL_id43 *= ComputeTextureProjection_id51(streams.PositionWS_id21.xyz, lightIndex);
    float3 reflectionVectorWS = reflect(-streams.viewWS_id66, streams.normalWS_id18);
    streams.lightSpecularColorNdotL_id44 *= ComputeSpecularTextureProjection_id52(streams.PositionWS_id21.xyz, reflectionVectorWS, lightIndex);
}
int GetLightCount_id162(inout PS_STREAMS streams)
{
    return streams.lightData_id89.y & 0xFFFF;
}
int GetMaxLightCount_id161(inout PS_STREAMS streams)
{
    return streams.lightData_id89.y & 0xFFFF;
}
void PrepareDirectLights_id160(inout PS_STREAMS streams)
{
    PrepareLightData_id56(streams);
}
void PrepareForLightingAndShading_id335()
{
}
void PrepareForLightingAndShading_id316()
{
}
void PrepareMaterialForLightingAndShading_id146(inout PS_STREAMS streams)
{
    streams.lightDirectAmbientOcclusion_id48 = lerp(1.0, streams.matAmbientOcclusion_id55, streams.matAmbientOcclusionDirectLightingFactor_id56);
    streams.matDiffuseVisible_id67 = streams.matDiffuse_id51.rgb * lerp(1.0f, streams.matCavity_id57, streams.matCavityDiffuse_id58) * streams.matDiffuseSpecularAlphaBlend_id63.r * streams.matAlphaBlendColor_id64;
    streams.matSpecularVisible_id69 = streams.matSpecular_id53.rgb * streams.matSpecularIntensity_id54 * lerp(1.0f, streams.matCavity_id57, streams.matCavitySpecular_id59) * streams.matDiffuseSpecularAlphaBlend_id63.g * streams.matAlphaBlendColor_id64;
    streams.NdotV_id70 = max(dot(streams.normalWS_id18, streams.viewWS_id66), 0.0001f);
    float roughness = 1.0f - streams.matGlossiness_id52;
    streams.alphaRoughness_id68 = max(roughness * roughness, 0.001);
}
void ResetLightStream_id145(inout PS_STREAMS streams)
{
    streams.lightPositionWS_id40 = 0;
    streams.lightDirectionWS_id41 = 0;
    streams.lightColor_id42 = 0;
    streams.lightColorNdotL_id43 = 0;
    streams.lightSpecularColorNdotL_id44 = 0;
    streams.envLightDiffuseColor_id45 = 0;
    streams.envLightSpecularColor_id46 = 0;
    streams.lightDirectAmbientOcclusion_id48 = 1.0f;
    streams.NdotL_id47 = 0;
}
void UpdateNormalFromTangentSpace_id23(float3 normalInTangentSpace)
{
}
float4 Compute_id187()
{
    return float4(constantFloat_id104, constantFloat_id104, constantFloat_id104, constantFloat_id104);
}
float4 Compute_id185()
{
    return float4(constantFloat_id101, constantFloat_id101, constantFloat_id101, constantFloat_id101);
}
float4 Compute_id183()
{
    return constantColor_id99;
}
void ResetStream_id148(inout PS_STREAMS streams)
{
    ResetStream_id147();
    streams.matBlend_id39 = 0.0f;
}
void Compute_id399(inout PS_STREAMS streams)
{
    UpdateNormalFromTangentSpace_id23(streams.matNormal_id49);
    if (!streams.IsFrontFace_id1)
        streams.normalWS_id18 = -streams.normalWS_id18;
    ResetLightStream_id145(streams);
    PrepareMaterialForLightingAndShading_id146(streams);

    {
        PrepareForLightingAndShading_id316();
    }

    {
        PrepareForLightingAndShading_id335();
    }
    float3 directLightingContribution = 0;

    {
        PrepareDirectLights_id160(streams);
        const int maxLightCount = GetMaxLightCount_id161(streams);
        int count = GetLightCount_id162(streams);

        for (int i = 0; i < maxLightCount; i++)
        {
            if (i >= count)
            {
                break;
            }
            PrepareDirectLight_id157(streams, i);
            PrepareMaterialPerDirectLight_id30(streams);

            {
                directLightingContribution += ComputeDirectLightContribution_id311(streams);
            }

            {
                directLightingContribution += ComputeDirectLightContribution_id367(streams);
            }
        }
    }

    {
        PrepareDirectLights_id168();
        const int maxLightCount = GetMaxLightCount_id173(streams);
        int count = GetLightCount_id174(streams);

        for (int i = 0; i < maxLightCount; i++)
        {
            if (i >= count)
            {
                break;
            }
            PrepareDirectLight_id170(streams, i);
            PrepareMaterialPerDirectLight_id30(streams);

            {
                directLightingContribution += ComputeDirectLightContribution_id311(streams);
            }

            {
                directLightingContribution += ComputeDirectLightContribution_id367(streams);
            }
        }
    }
    float3 environmentLightingContribution = 0;

    {
        PrepareEnvironmentLight_id179(streams);

        {
            environmentLightingContribution += ComputeEnvironmentLightContribution_id312(streams);
        }

        {
            environmentLightingContribution += ComputeEnvironmentLightContribution_id368(streams);
        }
    }

    {
        PrepareEnvironmentLight_id182(streams);

        {
            environmentLightingContribution += ComputeEnvironmentLightContribution_id312(streams);
        }

        {
            environmentLightingContribution += ComputeEnvironmentLightContribution_id368(streams);
        }
    }
    streams.shadingColor_id71 += directLightingContribution * PI_id110 + environmentLightingContribution;
    streams.shadingColorAlpha_id72 = streams.matDiffuse_id51.a;

    {
        AfterLightingAndShading_id319();
    }

    {
        AfterLightingAndShading_id338();
    }
}
void Compute_id383(inout PS_STREAMS streams)
{
    float metalness = Compute_id187().r;
    streams.matSpecular_id53 = lerp(0.02, streams.matDiffuse_id51.rgb, metalness);
    streams.matDiffuse_id51.rgb = lerp(streams.matDiffuse_id51.rgb, 0, metalness);
}
void Compute_id378(inout PS_STREAMS streams)
{
    float glossiness = Compute_id185().r;
    streams.matGlossiness_id52 = glossiness;
}
void Compute_id373(inout PS_STREAMS streams)
{
    float4 colorBase = Compute_id183();
    streams.matDiffuse_id51 = colorBase;
    streams.matColorBase_id50 = colorBase;
}
void ResetStream_id149(inout PS_STREAMS streams)
{
    ResetStream_id148(streams);
    streams.matNormal_id49 = float3(0, 0, 1);
    streams.matColorBase_id50 = 0.0f;
    streams.matDiffuse_id51 = 0.0f;
    streams.matDiffuseVisible_id67 = 0.0f;
    streams.matSpecular_id53 = 0.0f;
    streams.matSpecularVisible_id69 = 0.0f;
    streams.matSpecularIntensity_id54 = 1.0f;
    streams.matGlossiness_id52 = 0.0f;
    streams.alphaRoughness_id68 = 1.0f;
    streams.matAmbientOcclusion_id55 = 1.0f;
    streams.matAmbientOcclusionDirectLightingFactor_id56 = 0.0f;
    streams.matCavity_id57 = 1.0f;
    streams.matCavityDiffuse_id58 = 0.0f;
    streams.matCavitySpecular_id59 = 0.0f;
    streams.matEmissive_id60 = 0.0f;
    streams.matEmissiveIntensity_id61 = 0.0f;
    streams.matScatteringStrength_id62 = 1.0f;
    streams.matDiffuseSpecularAlphaBlend_id63 = 1.0f;
    streams.matAlphaBlendColor_id64 = 1.0f;
    streams.matAlphaDiscard_id65 = 0.1f;
}
float4 ComputeShadingPosition_id11(float4 world)
{
    return mul(world, ViewProjection_id28);
}
void PostTransformPosition_id6()
{
}
void PreTransformPosition_id4()
{
}
void Compute_id46(inout PS_STREAMS streams)
{

    {
        Compute_id373(streams);
    }

    {
        Compute_id378(streams);
    }

    {
        Compute_id383(streams);
    }

    {
        Compute_id399(streams);
    }
}
void ResetStream_id45(inout PS_STREAMS streams)
{
    ResetStream_id149(streams);
    streams.shadingColorAlpha_id72 = 1.0f;
}
void PostTransformPosition_id12(inout VS_STREAMS streams)
{
    PostTransformPosition_id6();
    streams.ShadingPosition_id0 = ComputeShadingPosition_id11(streams.PositionWS_id21);
    streams.PositionH_id23 = streams.ShadingPosition_id0;
    streams.DepthVS_id22 = streams.ShadingPosition_id0.w;
}
void TransformPosition_id5()
{
}
void PreTransformPosition_id10(inout VS_STREAMS streams)
{
    PreTransformPosition_id4();
    streams.PositionWS_id21 = mul(streams.Position_id20, World_id31);
}
float4 Shading_id31(inout PS_STREAMS streams)
{
    streams.viewWS_id66 = normalize(Eye_id30.xyz - streams.PositionWS_id21.xyz);
    streams.shadingColor_id71 = 0;
    ResetStream_id45(streams);
    Compute_id46(streams);
    return float4(streams.shadingColor_id71, streams.shadingColorAlpha_id72);
}
void PSMain_id1()
{
}
void BaseTransformVS_id8(inout VS_STREAMS streams)
{
    PreTransformPosition_id10(streams);
    TransformPosition_id5();
    PostTransformPosition_id12(streams);
}
void VSMain_id0()
{
}
void PSMain_id3(inout PS_STREAMS streams)
{
    PSMain_id1();
    streams.ColorTarget_id2 = Shading_id31(streams);
}
void GenerateNormal_PS_id22(inout PS_STREAMS streams)
{
    if (dot(streams.normalWS_id18, streams.normalWS_id18) > 0)
        streams.normalWS_id18 = normalize(streams.normalWS_id18);
    streams.meshNormalWS_id16 = streams.normalWS_id18;
}
void GenerateNormal_VS_id21(inout VS_STREAMS streams)
{
    streams.meshNormalWS_id16 = mul(streams.meshNormal_id15, (float3x3)WorldInverseTranspose_id33);
    streams.normalWS_id18 = streams.meshNormalWS_id16;
}
void VSMain_id9(inout VS_STREAMS streams)
{
    VSMain_id0();
    BaseTransformVS_id8(streams);
}
void PSMain_id20(inout PS_STREAMS streams)
{
    GenerateNormal_PS_id22(streams);
    PSMain_id3(streams);
}
void VSMain_id19(inout VS_STREAMS streams)
{
    VSMain_id9(streams);
    GenerateNormal_VS_id21(streams);
}
PS_OUTPUT PSMain(PS_INPUT __input__)
{
    PS_STREAMS streams = (PS_STREAMS)0;
    streams.PositionWS_id21 = __input__.PositionWS_id21;
    streams.ShadingPosition_id0 = __input__.ShadingPosition_id0;
    streams.normalWS_id18 = __input__.normalWS_id18;
    streams.ScreenPosition_id83 = __input__.ScreenPosition_id83;
    streams.IsFrontFace_id1 = __input__.IsFrontFace_id1;
    streams.ScreenPosition_id83 /= streams.ScreenPosition_id83.w;
    PSMain_id20(streams);
    PS_OUTPUT __output__ = (PS_OUTPUT)0;
    __output__.ColorTarget_id2 = streams.ColorTarget_id2;
    return __output__;
}
VS_OUTPUT VSMain(VS_INPUT __input__)
{
    VS_STREAMS streams = (VS_STREAMS)0;
    streams.Position_id20 = __input__.Position_id20;
    streams.meshNormal_id15 = __input__.meshNormal_id15;
    VSMain_id19(streams);
    streams.ScreenPosition_id83 = streams.ShadingPosition_id0;
    VS_OUTPUT __output__ = (VS_OUTPUT)0;
    __output__.PositionWS_id21 = streams.PositionWS_id21;
    __output__.ShadingPosition_id0 = streams.ShadingPosition_id0;
    __output__.normalWS_id18 = streams.normalWS_id18;
    __output__.ScreenPosition_id83 = streams.ScreenPosition_id83;
    return __output__;
}
