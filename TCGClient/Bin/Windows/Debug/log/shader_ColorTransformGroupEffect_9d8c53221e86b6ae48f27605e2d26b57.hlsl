/**************************
***** Compiler Parameters *****
***************************
@P EffectName: ColorTransformGroupEffect
@P   - ColorTransformGroup.Transforms: Xenko.Rendering.Images.ToneMap
***************************
****  ConstantBuffers  ****
***************************
cbuffer PerDraw [Size: 64]
@C    MatrixTransform_id73 => SpriteBase.MatrixTransform
cbuffer Globals [Size: 108]
@C    Texture0TexelSize_id15 => Texturing.Texture0TexelSize
@C    Texture1TexelSize_id17 => Texturing.Texture1TexelSize
@C    Texture2TexelSize_id19 => Texturing.Texture2TexelSize
@C    Texture3TexelSize_id21 => Texturing.Texture3TexelSize
@C    Texture4TexelSize_id23 => Texturing.Texture4TexelSize
@C    Texture5TexelSize_id25 => Texturing.Texture5TexelSize
@C    Texture6TexelSize_id27 => Texturing.Texture6TexelSize
@C    Texture7TexelSize_id29 => Texturing.Texture7TexelSize
@C    Texture8TexelSize_id31 => Texturing.Texture8TexelSize
@C    Texture9TexelSize_id33 => Texturing.Texture9TexelSize
@C    WhitePoint_id75 => ToneMapHejl2OperatorShader.WhitePoint.ToneMapOperator.Transforms[0]
@C    KeyValue_id80 => ToneMapShader.KeyValue.Transforms[0]
@C    LuminanceLocalFactor_id81 => ToneMapShader.LuminanceLocalFactor.Transforms[0]
@C    LuminanceAverageGlobal_id82 => ToneMapShader.LuminanceAverageGlobal.Transforms[0]
@C    Contrast_id83 => ToneMapShader.Contrast.Transforms[0]
@C    Brightness_id84 => ToneMapShader.Brightness.Transforms[0]
@C    Exposure_id85 => ToneMapShader.Exposure.Transforms[0]
***************************
******  Resources    ******
***************************
@R    PerDraw => PerDraw [Stage: Vertex, Slot: (-1--1)]
@R    Globals => Globals [Stage: Pixel, Slot: (-1--1)]
@R    Texture0_id14 => Texturing.Texture0 [Stage: Pixel, Slot: (-1--1)]
@R    Sampler_id42 => Texturing.Sampler [Stage: Pixel, Slot: (-1--1)]
***************************
*****     Sources     *****
***************************
@S    ColorTransformGroupShader => a1d206122071e59a4bec1533019c62cd
@S    ImageEffectShader => 26660dbf2eac041d293211cd6bbc17bb
@S    SpriteBase => e319c4777a964c3ebb8f82590e4e27f1
@S    ShaderBase => acbe3d4d44a046eede871176bee9c754
@S    ShaderBaseStream => a3a5bf8185f2a3d89972293f806430d3
@S    Texturing => e6daef0dd90a55f9549c6f5d291e61a5
@S    ColorTransformShader => da5abe1577ee1c52845f900ea5fb747e
@S    ToneMapShader => 8a332d80833ca0f7cd138cc0defd4a60
@S    ToneMapOperatorShader => abbefd9e902c50e7cf52ec6ea1777b27
@S    ToneMapHejl2OperatorShader => 5c5c1c8a727a0d12e744524a04156847
***************************
*****     Stages      *****
***************************
@G    Vertex => ee21b81a45ad187b1db8638166bf8552
@G    Pixel => 37e9ee28814e4964cba8aae6a044ce6e
***************************
*************************/
const static bool TAutoKeyValue_id76 = true;
const static bool TAutoExposure_id77 = true;
const static bool TUseLocalLuminance_id78 = false;
struct PS_STREAMS 
{
    float2 TexCoord_id62;
    float4 ColorTarget_id2;
};
struct PS_OUTPUT 
{
    float4 ColorTarget_id2 : SV_Target0;
};
struct PS_INPUT 
{
    float4 ShadingPosition_id0 : SV_Position;
    float2 TexCoord_id62 : TEXCOORD0;
};
struct VS_STREAMS 
{
    float4 Position_id72;
    float2 TexCoord_id62;
    float4 ShadingPosition_id0;
};
struct VS_OUTPUT 
{
    float4 ShadingPosition_id0 : SV_Position;
    float2 TexCoord_id62 : TEXCOORD0;
};
struct VS_INPUT 
{
    float4 Position_id72 : POSITION;
    float2 TexCoord_id62 : TEXCOORD0;
};
cbuffer PerDraw 
{
    float4x4 MatrixTransform_id73;
};
cbuffer Globals 
{
    float2 Texture0TexelSize_id15;
    float2 Texture1TexelSize_id17;
    float2 Texture2TexelSize_id19;
    float2 Texture3TexelSize_id21;
    float2 Texture4TexelSize_id23;
    float2 Texture5TexelSize_id25;
    float2 Texture6TexelSize_id27;
    float2 Texture7TexelSize_id29;
    float2 Texture8TexelSize_id31;
    float2 Texture9TexelSize_id33;
    float WhitePoint_id75 = 5.0f;
    float KeyValue_id80 = 0.18f;
    float LuminanceLocalFactor_id81 = 0.0f;
    float LuminanceAverageGlobal_id82;
    float Contrast_id83 = 0.0f;
    float Brightness_id84 = 0.0f;
    float Exposure_id85 = 1.0f;
};
Texture2D Texture0_id14;
Texture2D Texture1_id16;
Texture2D Texture2_id18;
Texture2D Texture3_id20;
Texture2D Texture4_id22;
Texture2D Texture5_id24;
Texture2D Texture6_id26;
Texture2D Texture7_id28;
Texture2D Texture8_id30;
Texture2D Texture9_id32;
TextureCube TextureCube0_id34;
TextureCube TextureCube1_id35;
TextureCube TextureCube2_id36;
TextureCube TextureCube3_id37;
Texture3D Texture3D0_id38;
Texture3D Texture3D1_id39;
Texture3D Texture3D2_id40;
Texture3D Texture3D3_id41;
SamplerState Sampler_id42;
SamplerState PointSampler_id43 
{
    Filter = MIN_MAG_MIP_POINT;
};
SamplerState LinearSampler_id44 
{
    Filter = MIN_MAG_MIP_LINEAR;
};
SamplerState LinearBorderSampler_id45 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Border;
    AddressV = Border;
};
SamplerComparisonState LinearClampCompareLessEqualSampler_id46 
{
    Filter = COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
    AddressU = Clamp;
    AddressV = Clamp;
    ComparisonFunc = LessEqual;
};
SamplerState AnisotropicSampler_id47 
{
    Filter = ANISOTROPIC;
};
SamplerState AnisotropicRepeatSampler_id48 
{
    Filter = ANISOTROPIC;
    AddressU = Wrap;
    AddressV = Wrap;
    MaxAnisotropy = 16;
};
SamplerState PointRepeatSampler_id49 
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState LinearRepeatSampler_id50 
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState RepeatSampler_id51 
{
    AddressU = Wrap;
    AddressV = Wrap;
};
SamplerState Sampler0_id52;
SamplerState Sampler1_id53;
SamplerState Sampler2_id54;
SamplerState Sampler3_id55;
SamplerState Sampler4_id56;
SamplerState Sampler5_id57;
SamplerState Sampler6_id58;
SamplerState Sampler7_id59;
SamplerState Sampler8_id60;
SamplerState Sampler9_id61;
Texture2D LuminanceTexture_id79;
float4 Compute_id8(float4 color)
{
    float w = (1.425 * WhitePoint_id75) + 0.05f;
    w = ((WhitePoint_id75 * w + 0.004f) / ((WhitePoint_id75 * (w + 0.55f) + 0.0491f))) - 0.0821f;
    float4 vh = float4(color.rgb, WhitePoint_id75);
    float4 va = (1.425 * vh) + 0.05f;
    float4 vf = ((vh * va + 0.004f) / ((vh * (va + 0.55f) + 0.0491f))) - 0.0821f;
    return float4(vf.rgb / w, 1.0);
}
float CalculateExposure_id6(float avgLuminance)
{
    float exposure;

    {
        float keyValue;

        {
            keyValue = 1.03f - (2.0f / (2 + log10(avgLuminance + 1)));
        }
        float linearExposure = (keyValue / avgLuminance);
        exposure = max(linearExposure, 0.0001f);
    }
    return exposure;
}
float3 ToneMap_id5(float3 color, float avgLuminance)
{
    float exposure = CalculateExposure_id6(avgLuminance);
    color *= exposure;
    color = Compute_id8(float4(color, 1)).rgb;
    return color;
}
float4 Compute_id4(inout PS_STREAMS streams, float4 inputColor)
{
    float3 color = inputColor.rgb;
    float avgLuminance = LuminanceAverageGlobal_id82;
    avgLuminance = exp2(avgLuminance);
    avgLuminance = max(avgLuminance, 0.0001f);
    float globalAverageLum = exp2(LuminanceAverageGlobal_id82);
    color = max(color + globalAverageLum.xxx * Brightness_id84, 0.0001);
    color = max(lerp(globalAverageLum.xxx, color, Contrast_id83 + 1.0f), 0.0001);
    color = ToneMap_id5(color, avgLuminance);
    return float4(color, inputColor.a);
}
float4 Shading_id2(inout PS_STREAMS streams)
{
    return Texture0_id14.Sample(Sampler_id42, streams.TexCoord_id62);
}
float4 Shading_id3(inout PS_STREAMS streams)
{
    float4 color = Shading_id2(streams);

    {
        color = Compute_id4(streams, color);
    }
    return color;
}
PS_OUTPUT PSMain(PS_INPUT __input__)
{
    PS_STREAMS streams = (PS_STREAMS)0;
    streams.TexCoord_id62 = __input__.TexCoord_id62;
    streams.ColorTarget_id2 = Shading_id3(streams);
    PS_OUTPUT __output__ = (PS_OUTPUT)0;
    __output__.ColorTarget_id2 = streams.ColorTarget_id2;
    return __output__;
}
VS_OUTPUT VSMain(VS_INPUT __input__)
{
    VS_STREAMS streams = (VS_STREAMS)0;
    streams.Position_id72 = __input__.Position_id72;
    streams.TexCoord_id62 = __input__.TexCoord_id62;
    streams.ShadingPosition_id0 = mul(streams.Position_id72, MatrixTransform_id73);
    VS_OUTPUT __output__ = (VS_OUTPUT)0;
    __output__.ShadingPosition_id0 = streams.ShadingPosition_id0;
    __output__.TexCoord_id62 = streams.TexCoord_id62;
    return __output__;
}
